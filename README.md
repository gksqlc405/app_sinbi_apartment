# 아파트 사용자 관리 APP

***

#### LANGUAGE
```
Dart 
Flutter
```
#### 기능
```

* 로그인 관리
  - Security
  - 사용자 로그인(token) (Post)
  
* 관리비(사용자)
  - 월별내역서 가저오기 (Get)
  
* 민원 창구(사용자)
  - 민원창구 상세페이지 가저오기 (Get)
  - 민원창구 완료 리스트 가저오기(Get)
  - 민원창구 진행 리스트 가저오기(Get)
  - 민원창구 접수 리스트 가저오기(Get)
  - 민원창구 모든민원 리스트 가저오기(Get)
  - 민원창구 게시글 등록 (Post)
  - 민원창구 댓글 리스트 가저오기 (Get)
  - 민원창구 댓글 등록 (Post)

* 방문 차량 (사용자)
  - 방문차량 신청내역 조회 (Get)
  - 방문차량 등록 (Post)
  
* 세대원 (사용자)
  - 세대원 정보 가저오기 (Get)
  
* 아파트 소식 (사용자)
  - 아파트 소식 완료 리스트 가저오기 (Get)
  - 아파트 소식 진행 리스트 가저오기 (Get)
  - 아파트 소식 예정 리스트 가저오기 (Get)
  - 아파트 소식 상세정보 가저오기 (Get)
  - 아파트 소식 전체 리스트 가저오기 (Get)
  - 아파트 소식 댓글 리스트 가저오기 (Get)
  - 아파트 소식 댓글 등록 (Post)
 
* 입주민 라운지 (사용자)
  - 카테고리 게시글 상세페이지 가저오기 (Get)
  - 카테고리 리스트 가저오기 (Get)
  - 카테고리 게시글 리스트 가저오기 (Get)
  - 카테고리 게시글 등록 (Post)
  - 카테고리 게시글 댓글 등록 (Post) 
  - 카테고리 게시글 댓글 리스트 가저오기 (Get)
 
* 입주민 차량 
  - 입주민 차량 리스트 가저오기 (Get)
  
* 입주민 관리
  - 입주민 상세정보 가저오기 (Get)

```


# app 화면

### 로그인 화면
![login](./images/login.png)

### 메인 화면
![Main](./images/main.png)

### 민원 창구
![Complaint](./images/min.png)

### 아파트소식
![Apt](./images/apt.png)

### 입주민 라운지
![Category](./images/category.png)
![Category](./images/category1.png)

### 방문차량
![VisitCar](./images/visitcar.png)

### 우리집
![Home](./images/home.png)

### 로그아웃
![Home](./images/logout.png)
