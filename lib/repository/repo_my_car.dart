import 'package:app_sinbi_apartment/config/config_api.dart';
import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/model/my_car_list_result.dart';
import 'package:dio/dio.dart';

class RepoMyCar {


  Future<MyCarListResult> getMyCarList() async {
    const String baseUrl = '$apiUrl/my-car/list';


    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return  status == 200;
            }
        )
    );

    return MyCarListResult.fromJson(response.data);
  }
}