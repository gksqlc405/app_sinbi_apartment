import 'package:app_sinbi_apartment/config/config_api.dart';
import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/model/common_result.dart';
import 'package:app_sinbi_apartment/model/meeting_bulletin_detail_response.dart';
import 'package:app_sinbi_apartment/model/meeting_bulletin_list_result.dart';
import 'package:app_sinbi_apartment/model/meeting_comment_list_result.dart';
import 'package:app_sinbi_apartment/model/meeting_comment_request.dart';
import 'package:app_sinbi_apartment/model/meeting_list_result.dart';
import 'package:app_sinbi_apartment/model/meeting_request.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

class RepoMeeting {


  Future<CommonResult> setMeeting(String category,MeetingRequest request) async {
    const String baseUrl = '$apiUrl/meeting/new/meeting/{category}';

    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['AUTHORIZATION'] = 'Bearer ' + token!;

    final response = await dio.post(
        baseUrl.replaceAll('{category}', category.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }

  Future<MeetingListResult> getMeetingList() async {
    const String baseUrl = '$apiUrl/meeting/list';

    String? token = await TokenLib.getToken();
    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return  status == 200;
            }
        )
    );

    return MeetingListResult.fromJson(response.data);
  }

  Future<MeetingBulletinListResult> getBulletinList(String category) async {
    const String baseUrl = '$apiUrl/meeting/list/getClimbing/{category}';

    String? token = await TokenLib.getToken();
    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(
        baseUrl.replaceAll('{category}', category.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return  status == 200;
            }
        )
    );

    return MeetingBulletinListResult.fromJson(response.data);
  }

  Future<MeetingBulletinDetailResponse> getBulletinDetail(int meetingId) async {
    const String baseUrl = '$apiUrl/meeting/{meetingId}';

    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['AUTHORIZATION'] = 'Bearer ' + token!;

    final response = await dio.get(
      baseUrl.replaceAll('{meetingId}', meetingId.toString()),
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          }
      ),
    );

    return MeetingBulletinDetailResponse.fromJson(response.data);
  }


  Future<MeetingCommentListResult> getMeetingCommentList(int meetingId) async {
    const String baseUrl = '$apiUrl/meeting-comment/getComment/{meetingId}';

    String? token = await TokenLib.getToken();
    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(
        baseUrl.replaceAll('{meetingId}', meetingId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return  status == 200;
            }
        )
    );

    return MeetingCommentListResult.fromJson(response.data);
  }

  Future<CommonResult> setMeetingComment(int meetingId,MeetingCommentRequest request) async {
    const String baseUrl = '$apiUrl/meeting-comment/comment/{meetingId}';

    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['AUTHORIZATION'] = 'Bearer ' + token!;

    final response = await dio.post(
        baseUrl.replaceAll('{meetingId}', meetingId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }
}