import 'package:app_sinbi_apartment/config/config_api.dart';
import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/model/common_result.dart';
import 'package:app_sinbi_apartment/model/complain_comment_request.dart';
import 'package:app_sinbi_apartment/model/complain_comments_list_item.dart';
import 'package:app_sinbi_apartment/model/complain_comments_list_result.dart';
import 'package:app_sinbi_apartment/model/complain_detail_response.dart';
import 'package:app_sinbi_apartment/model/complain_list_result.dart';
import 'package:app_sinbi_apartment/model/complain_request.dart';
import 'package:app_sinbi_apartment/model/complain_state_list_result.dart';
import 'package:dio/dio.dart';

class RepoComplain {

  Future<ComplainListResult> getList() async {
    const String baseUrl = '$apiUrl/complain/list';

    String? token = await TokenLib.getToken();
    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(
        baseUrl,
        options: Options(
          followRedirects: false,
          validateStatus: (status) {
          return  status == 200;
          }
        )
    );

    return ComplainListResult.fromJson(response.data);
  }

  Future<ComplainDetailResponse> getComplainDetail(int complainId) async {
    const String baseUrl = '$apiUrl/complain/complain/{complainId}';

    String? token = await TokenLib.getToken();
    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ' + token!;


    final response = await dio.get(
      baseUrl.replaceAll('{complainId}', complainId.toString()),
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          }
      ),
    );

    return ComplainDetailResponse.fromJson(response.data);
  }


  Future<ComplainCommentsListResult> getComplainCommentList(int complainId) async {
    const String baseUrl = '$apiUrl/complain-comments/list/{complainId}';



    String? token = await TokenLib.getToken();
    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ' + token!;


    final response = await dio.get(
        baseUrl.replaceAll('{complainId}', complainId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return  status == 200;
            }
        )
    );

    return ComplainCommentsListResult.fromJson(response.data);
  }


  Future<ComplainStateListResult> getRegisterList() async {
    const String baseUrl = '$apiUrl/complain/register';

    String? token = await TokenLib.getToken();
    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return  status == 200;
            }
        )
    );

    return ComplainStateListResult.fromJson(response.data);
  }

  Future<ComplainStateListResult> getProgressList() async {
    const String baseUrl = '$apiUrl/complain/progress';

    String? token = await TokenLib.getToken();
    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return  status == 200;
            }
        )
    );

    return ComplainStateListResult.fromJson(response.data);
  }

  Future<ComplainStateListResult> getCompletionList() async {
    const String baseUrl = '$apiUrl/complain/completion';

    String? token = await TokenLib.getToken();
    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return  status == 200;
            }
        )
    );

    return ComplainStateListResult.fromJson(response.data);
  }

  Future<CommonResult> setComplain(ComplainRequest request) async {
    const String baseUrl = '$apiUrl/complain/new/complain';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['AUTHORIZATION'] = 'Bearer ' + token!;

    final response = await dio.post(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> setComplainComment(int complainId,ComplainCommentRequest request) async {
    const String baseUrl = '$apiUrl/complain-comments/resident/complain/{complainId}';

    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['AUTHORIZATION'] = 'Bearer ' + token!;

    final response = await dio.post(
        baseUrl.replaceAll('{complainId}', complainId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }
}