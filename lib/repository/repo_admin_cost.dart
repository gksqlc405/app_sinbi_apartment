import 'package:app_sinbi_apartment/config/config_api.dart';
import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/model/admin_cost_detail_response.dart';
import 'package:dio/dio.dart';

class RepoAdminCost {

  Future<AdminCostDetailResponse> getAdminCostDetail( int year, int month) async {
    const String baseUrl = '$apiUrl/manager/adminCost/year/{year}/month/{month}';

    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(
      baseUrl.replaceAll('{year}', year.toString()).replaceAll('{month}', month.toString()),
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          }
      ),
    );

    return AdminCostDetailResponse.fromJson(response.data);
  }
}