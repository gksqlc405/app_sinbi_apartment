import 'package:app_sinbi_apartment/config/config_api.dart';
import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/model/common_result.dart';
import 'package:app_sinbi_apartment/model/news_comment_list_result.dart';
import 'package:app_sinbi_apartment/model/news_comments_request.dart';
import 'package:app_sinbi_apartment/model/news_detail_response.dart';
import 'package:app_sinbi_apartment/model/news_title_list_result.dart';
import 'package:dio/dio.dart';

class RepoNews {

  Future<NewsDetailResponse> getNewsDetail(int newsId) async {
    const String baseUrl = '$apiUrl/news/news/{newsId}';

    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['AUTHORIZATION'] = 'Bearer ' + token!;

    final response = await dio.get(
      baseUrl.replaceAll('{newsId}', newsId.toString()),
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          }
      ),
    );

    return NewsDetailResponse.fromJson(response.data);
  }

  Future<NewsTitleListResult> getNewsTitleList() async {
    const String baseUrl = '$apiUrl/news/newsList';

    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['AUTHORIZATION'] = 'Bearer ' + token!;

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return NewsTitleListResult.fromJson(response.data);
  }


  Future<NewsTitleListResult> getNewsScheduleList() async {
    const String baseUrl = '$apiUrl/news/schedule';

    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['AUTHORIZATION'] = 'Bearer ' + token!;

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return NewsTitleListResult.fromJson(response.data);
  }


  Future<NewsTitleListResult> getProgressList() async {
    const String baseUrl = '$apiUrl/news/progress';

    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['AUTHORIZATION'] = 'Bearer ' + token!;

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return NewsTitleListResult.fromJson(response.data);
  }


  Future<NewsTitleListResult> getCompletionList() async {
    const String baseUrl = '$apiUrl/news/completion';

    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['AUTHORIZATION'] = 'Bearer ' + token!;

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return NewsTitleListResult.fromJson(response.data);
  }

  Future<NewsCommentListResult> getNewsCommentList(int newsId) async {
    const String baseUrl = '$apiUrl/news-comments/list/{newsId}';


    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['AUTHORIZATION'] = 'Bearer ' + token!;

    final response = await dio.get(
        baseUrl.replaceAll('{newsId}', newsId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return  status == 200;
            }
        )
    );

    return NewsCommentListResult.fromJson(response.data);
  }




  Future<CommonResult> setNewsComment(int newsId,NewsCommentsRequest request) async {
    const String baseUrl = '$apiUrl/news-comments/resident/news/{newsId}';

    String? token = await TokenLib.getToken();
    Dio dio = Dio();
    dio.options.headers['AUTHORIZATION'] = 'Bearer ' + token!;

    final response = await dio.post(
        baseUrl.replaceAll('{newsId}', newsId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }

}