import 'package:app_sinbi_apartment/config/config_api.dart';
import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/model/common_result.dart';
import 'package:app_sinbi_apartment/model/visit_car_list_result.dart';
import 'package:app_sinbi_apartment/model/visit_car_request.dart';
import 'package:dio/dio.dart';

class RepoVisitCar {

  Future<CommonResult> setVisitCar(VisitCarRequest request) async {
    const String baseUrl = '$apiUrl/visit-car/new/visitCar';
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['AUTHORIZATION'] = 'Bearer ' + token!;

    final response = await dio.post(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }

  Future<VisitCarListResult> getVisitCarList() async {
    const String baseUrl = '$apiUrl/visit-car/list/visitCar';


    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['AUTHORIZATION'] = 'Bearer ' + token!;

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return  status == 200;
            }
        )
    );

    return VisitCarListResult.fromJson(response.data);
  }


  Future<CommonResult> delVisitCar(int visitCarId) async {
    const String baseUrl = '$apiUrl/visit-car/visitCar/{visitCarId}';


    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['AUTHORIZATION'] = 'Bearer ' + token!;

    final response = await dio.delete(
        baseUrl.replaceAll('{visitCarId}', visitCarId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return  status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

}
