import 'package:app_sinbi_apartment/config/config_api.dart';
import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/model/login_request.dart';
import 'package:app_sinbi_apartment/model/login_response_result.dart';
import 'package:app_sinbi_apartment/model/resident_detail_response.dart';
import 'package:dio/dio.dart';

class RepoResident {

  Future<LoginResponseResult> doLogin(LoginRequest request) async {
    const String baseUrl = '$apiUrl/login/app/user';

    Dio dio = Dio();

    final response = await dio.post(
      baseUrl,
      data: request.toJson(),
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          }
      )
    );
    return LoginResponseResult.fromJson(response.data);

  }


  Future<ResidentDetailResponse> getResidentDetail() async {
    const String baseUrl = '$apiUrl/resident/resident';

    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(
      baseUrl,
      options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          }
      ),
    );

    return ResidentDetailResponse.fromJson(response.data);
  }
}