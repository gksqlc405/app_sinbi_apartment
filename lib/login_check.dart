import 'package:app_sinbi_apartment/middleware/middleware_login_check.dart';
import 'package:flutter/material.dart';

class LoginCheck extends StatefulWidget {
  const LoginCheck({Key? key}) : super(key: key);

  @override
  State<LoginCheck> createState() => _LoginCheckState();
}

class _LoginCheckState extends State<LoginCheck> {
  void initState() {
    super.initState();
    MiddlewareLoginCheck().check(context);
  }


  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
