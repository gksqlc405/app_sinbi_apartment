
import 'package:app_sinbi_apartment/pages/page_complain_list.dart';
import 'package:app_sinbi_apartment/pages/page_my_house.dart';
import 'package:app_sinbi_apartment/pages/page_lounge.dart';
import 'package:app_sinbi_apartment/pages/page_visit_car.dart';
import 'package:app_sinbi_apartment/pages/page_we_apartment.dart';
import 'package:flutter/material.dart';

class PageMain extends StatefulWidget {
  const PageMain({Key? key}) : super(key: key);

  @override
  State<PageMain> createState() => _PageMainState();
}

class _PageMainState extends State<PageMain> {
  int _currentTabIndex = 0;



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    final _kTabPages = <Widget>[
      const Center(child: PageWeApartment()),
      const Center(child: PageLounge()),
      const Center(child: PageVisitCar()),
      const Center(child: PageMyHouse()),
    ];
    final _kBottmonNavBarItems = <BottomNavigationBarItem>[
      // const BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
      const BottomNavigationBarItem(icon: Icon(Icons.hail), label: '우리아파트',),
      const BottomNavigationBarItem(icon: Icon(Icons.favorite_border), label: '입주민 라운지'),
      const BottomNavigationBarItem(icon: Icon(Icons.person_pin), label: '방문차량'),
      const BottomNavigationBarItem(icon: Icon(Icons.person_pin), label: '우리집'),
    ];
    assert(_kTabPages.length == _kBottmonNavBarItems.length);
    final bottomNavBar = BottomNavigationBar(
      // selectedItemColor: colorMain,
      items: _kBottmonNavBarItems,
      currentIndex: _currentTabIndex,
      type: BottomNavigationBarType.fixed,
      onTap: (int index) {
        setState(() {
          _currentTabIndex = index;
        });
      },
    );
    return Scaffold(
      body: _kTabPages[_currentTabIndex],
      bottomNavigationBar: bottomNavBar,
    );
  }
}
