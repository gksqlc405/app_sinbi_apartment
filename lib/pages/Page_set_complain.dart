import 'package:app_sinbi_apartment/components/component_appbar_normal.dart';
import 'package:app_sinbi_apartment/components/component_custom_loading.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/config/config_color.dart';
import 'package:app_sinbi_apartment/config/config_form_validator.dart';
import 'package:app_sinbi_apartment/model/complain_request.dart';
import 'package:app_sinbi_apartment/pages/page_complain_list.dart';
import 'package:app_sinbi_apartment/repository/repo_complain.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageSetComplain extends StatefulWidget {
  const PageSetComplain({Key? key}) : super(key: key);



  @override
  State<PageSetComplain> createState() => _PageSetComplainState();
}

class _PageSetComplainState extends State<PageSetComplain> {


  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _setComplain(ComplainRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoComplain().setComplain(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '민원접수가 완료되었습니다',
        subTitle: '완료',
      ).call();

      Navigator.pop(
          context,
          [true]
      );
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false, title: '민원접수가 실패하였습니다', subTitle: '양식에 맞는지 확인해주십시오',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarNormal(
        title: '민원접수',
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Container(
          padding: EdgeInsets.all(5),
          margin: EdgeInsets.all(5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 20,),
              Container(
                margin: EdgeInsets.only(bottom: 15),
                child: Text("제목",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              FormBuilderTextField(
                maxLength: 15,
                name: 'title',
                decoration:  InputDecoration(
                  labelText: '내용을 입력해주세요.',
                  labelStyle: TextStyle(
                    fontSize: 13,
                  ),
                  suffixIcon: Text(
                    '2자이상~15이하   ',
                    style: TextStyle(
                      wordSpacing: 5,
                      height: 5,
                      fontSize: 10,
                      color: Colors.black,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: colorSilver),
                  ),
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  // err 이 값은 필수입니다
                  FormBuilderValidators.minLength(
                      2, errorText: formErrorMinLength(2)),
                  // err $num자 이상 입력해주세요
                  FormBuilderValidators.maxLength(
                      30, errorText: formErrorMaxLength(30)),
                  // err $num자 이하로 입력해주세요
                ]),
                keyboardType: TextInputType.text,
              ),
              SizedBox(height: 30,),
              Container(
                margin: EdgeInsets.only(bottom: 15),
                child: Text("내용",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              FormBuilderTextField(
                maxLength: 100,
                maxLines: 10,
                name: 'content',
                decoration: InputDecoration(

                  labelText: '민원접수 내용 입력해주세요.',
                  labelStyle: TextStyle(
                    fontSize: 20,
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: colorSilver),
                  ),
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  // err 이 값은 필수입니다
                  FormBuilderValidators.maxLength(
                      100, errorText: formErrorMaxLength(100)),
                  // err $num자 이하로 입력해주세요
                ]),
                keyboardType: TextInputType.text,
              ),
              SizedBox(height: 30,),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 15),
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(colorSilver),
                      ),
                        onPressed: () {
                          _showSetDialog();
                        }, child: Text('입력하기')),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

void _showSetDialog() {
  showDialog(context: context,
      barrierDismissible: true,  //뒷배경의 touchEvent 가능여
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('민원접수를 하시겠습니까?'), // 제목
          content: const Text('접수시 확인후 안내드립니다'),  // 부제목
          actions: [
            OutlinedButton(
                onPressed: () {
                  if (_formKey.currentState?.saveAndValidate() ?? false) {
                    ComplainRequest request = ComplainRequest(
                      _formKey.currentState!.fields['title']!.value,
                      _formKey.currentState!.fields['content']!.value,
                    );

                    Navigator.pop(
                      context,
                      [true],
                    );
                    _setComplain(request);
                  }
                },
                child: const Text('확인')
            ),
            OutlinedButton(
                onPressed: () {
                  Navigator.pop(context);  // 취소일땐 삭제하면 안돼기 때문에 pop
                },
                child: const Text('취소')
            ),
          ],
        );
      }
  );
}
}

