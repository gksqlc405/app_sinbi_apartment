import 'package:app_sinbi_apartment/components/component_bulletin_list.dart';
import 'package:app_sinbi_apartment/components/component_custom_loading.dart';
import 'package:app_sinbi_apartment/components/component_meeting_list.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/config/config_color.dart';
import 'package:app_sinbi_apartment/model/meeting_bulletin_list_item.dart';
import 'package:app_sinbi_apartment/model/meeting_list_item.dart';
import 'package:app_sinbi_apartment/pages/page_lounge_bulletin_detail.dart';
import 'package:app_sinbi_apartment/pages/page_set_bulletin.dart';
import 'package:app_sinbi_apartment/repository/repo_meeting.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageLoungeBulletin extends StatefulWidget {
  const PageLoungeBulletin({
    super.key, required this.categoryValue, required this.category, required this.image, required this.categoryTitle, required this.categorySubTitle});

 final String categoryValue;
 final String category;
 final String image;
 final String categoryTitle;
 final String categorySubTitle;

  @override
  State<PageLoungeBulletin> createState() => _PageLoungeBulletinState();
}

class _PageLoungeBulletinState extends State<PageLoungeBulletin> {
  final _scrollController = ScrollController();
  List<MeetingBulletinListItem> _list = [];

  @override
  void initState() {
    super.initState();
    _getBulletinList();

  }



  Future<void> _getBulletinList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoMeeting().getBulletinList(widget.categoryValue).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '실패',
      ).call();
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        controller: _scrollController,
        children: [
          Column(
            children: [
              Container(
                width: 300,
                padding: EdgeInsets.all(30),
                margin: EdgeInsets.fromLTRB(0, 40, 0, 10),
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: colorSinbi,
                      blurRadius: 5.0,
                      spreadRadius: 1.0,
                    ),
                  ],
                  border: Border.all(
                    color: colorSinbi,
                    style: BorderStyle.solid,
                    width: 5,
                  ),
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text('${widget.category}',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'kakaoBold',
                          ),
                        ),
                        Spacer(flex: 1,),
                        Image.asset(
                         'assets/${widget.image}', height: 100, width: 100,),
                      ],
                    ),
                    Text('${widget.categoryTitle}',
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'kakaoBold',
                      ),
                    ),
                    Text('${widget.categorySubTitle}',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'kakaoBold',
                    ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(150, 10, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    TextButton(onPressed: () async {
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              PageSetBulletin(categoryValue: widget.categoryValue,
                              ),
                        ),
                      );
                      setState(() {
                        _getBulletinList();
                      });
                    }, child: Text('<글쓰기> \u{270F}',
                      style: TextStyle(
                          color: Colors.white,
                        fontFamily: 'kakaoBold',
                      ),
                    ),
                    ),
                  ],
                ),
              ),
              _buildList(),
            ],
          ),
        ],
      ),
      backgroundColor: colorSinbiblue,

    );
  }


  Widget _buildList() {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) =>
                ComponentBulletinList(item: _list[index], callback: () async {
                  final popup = await Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) =>
                          PageLoungeBulletinDetail(meetingId: _list[index].meetingId, category: widget.category))
                  );

                  // 상세페이지에서 수정이나 삭제 완료하고 닫혔다!! 라고 알려주면 리스트 갱신하기
                  if (popup != null && popup[0]) {
                    _getBulletinList();
                  }
                },

                ),
          ),
        ],
      ),
    );
  }

}


