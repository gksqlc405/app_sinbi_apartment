import 'package:app_sinbi_apartment/components/component_appbar_normal.dart';
import 'package:app_sinbi_apartment/components/component_custom_loading.dart';
import 'package:app_sinbi_apartment/components/component_meeting_comment_list.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/config/config_color.dart';
import 'package:app_sinbi_apartment/model/meeting_bulletin_details.dart';
import 'package:app_sinbi_apartment/model/meeting_comment_list_item.dart';
import 'package:app_sinbi_apartment/pages/page_set_meeting_comment.dart';
import 'package:app_sinbi_apartment/repository/repo_meeting.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PageLoungeBulletinDetail extends StatefulWidget {
  const PageLoungeBulletinDetail({super.key, required this.meetingId, required this.category});

  final int meetingId;
  final String category;

  @override
  State<PageLoungeBulletinDetail> createState() => _PageLoungeBulletinDetailState();
}

class _PageLoungeBulletinDetailState extends State<PageLoungeBulletinDetail> {
  String content = '';

  final _scrollController = ScrollController();
  final NumberFormat nFormat = NumberFormat('#,###');

  MeetingBulletinDetails _detail = MeetingBulletinDetails(0,'', DateTime.now(), '', '', 0);

  List<MeetingCommentListItem> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;


  final DateFormat dateCreate = DateFormat('yyyy-MM-dd HH:mm:ss');



  @override
  void initState() {
    super.initState();
    _getBulletinDetail();
    _meetingCommentList();
  }

  Future<void> _meetingCommentList({bool reFresh = false}) async {
    if (reFresh) {
      _list = [];
      _currentPage = 1;
      _totalPage = 1;
      _totalItemCount = 0;
    }
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoMeeting().getMeetingCommentList(widget.meetingId).then((res) {
      BotToast.closeAllLoading();

      setState(() {

        _list = res.list;
        _totalItemCount = res.totalItemCount;

      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '실패',
      ).call();
    });

    if (reFresh) {
      _scrollController.animateTo(
          0, duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut);
    }
  }

  Future<void> _getBulletinDetail() async {

    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoMeeting().getBulletinDetail(widget.meetingId).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _detail = res.date;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();

      Navigator.pop(context);
    });


  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  ComponentAppbarNormal(
        title: widget.category,
      ),
      body: _buildBody(),
      bottomNavigationBar: BottomAppBar(
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(colorSinbi),
            minimumSize: MaterialStateProperty.all(Size(50, 50)),
          ),
          onPressed: () async {
             await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PageSetMeetingComment(
                      meetingId: widget.meetingId,
                    )
                )
            );
             setState(() {
               _meetingCommentList();
             });

          },
          child: Text(
            '댓글쓰기',
            style: TextStyle(
                fontFamily: 'kakaoBold',
                color: Colors.white,
                fontSize: 20
            ),
          ),
        ),
      ),
    );
  }
  Widget _buildBody() {
    return ListView(
      controller: _scrollController,
      children: [
        Center(
          child: Container(
            child: Column(crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 50,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(15, 0, 0, 0),
                      child:
                      Text('${      _detail.title}',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          fontFamily: 'kakaoBold',
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 15, 0),
                      child: Text(dateCreate.format(_detail.dateCreate),
                        style: TextStyle(
                          fontSize: 9,
                          color: colorSilver,
                          fontFamily: 'kakao',
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 10,),
                Row(
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(15, 0, 0, 0),
                      child: Text('${_detail.userName}',
                        style: TextStyle(
                            color: colorSilver,
                          fontSize: 12,
                          fontFamily: 'kakao',
                        ),
                      ),
                    ),
                    Text(' (${_detail.aptDong}동)',
                      style: TextStyle(
                          color: colorSilver,
                        fontSize: 12,
                        fontFamily: 'kakao',
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 70,),
                Container(
                  margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: Text('${_detail.content}',
                    style: TextStyle(
                      fontSize: 15,
                      fontFamily: 'kakao',
                    ),
                  ),
                ),
                SizedBox(height: 30,),
                Divider(thickness: 1,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(15, 5, 0, 5),
                      child: Text('댓글',
                      style: TextStyle(
                        fontFamily: 'kakao',
                      ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                      child: Text("${_totalItemCount}개",
                        style: TextStyle(
                          color: colorSinbiblue,
                          fontFamily: 'kakao',
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 10,),
                _buildList(),
              ],
            ),
          ),
        ),
      ],

    );

  }

  Widget _buildList() {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentMeetingCommentList(item: _list[index]
            ),
          ),
        ],
      ),
    );
  }

}