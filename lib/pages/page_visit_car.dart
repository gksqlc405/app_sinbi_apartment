import 'package:app_sinbi_apartment/components/component_custom_loading.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/config/config_color.dart';
import 'package:app_sinbi_apartment/model/visit_car_request.dart';
import 'package:app_sinbi_apartment/pages/page_visit_car_list.dart';
import 'package:app_sinbi_apartment/repository/repo_visit_car.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

import '../config/config_form_validator.dart';

class PageVisitCar extends StatefulWidget {
  const PageVisitCar({Key? key}) : super(key: key);


  @override
  State<PageVisitCar> createState() => _PageVisitCarState();
}

class _PageVisitCarState extends State<PageVisitCar> {
  final _formKey = GlobalKey<FormBuilderState>();

  String carNumber = '';




  Future<void> _setVisitCar(VisitCarRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoVisitCar().setVisitCar(request).then((res)  {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '방문차량 신청처리가 완료 되었습니다.',
        subTitle: '완료',
      ).call();




    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false, title: '방문차량 신청이 실패하였습니다', subTitle: '양식에 맞는지 확인해주십시오',
      ).call();
    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
              margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
              child:
              Text('\u{1F698} 방문차량',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                  fontFamily: 'kakaoBold',
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
              child:  Text('손님들에게 쾌적한 주차 공간을 위해\n방문차량을 등록해보세요!!',style: TextStyle(color: colorSilver,fontFamily: 'kakao',),),
            ),
            SizedBox(height: 50,),
            Container(alignment: Alignment.center,
              margin: EdgeInsets.fromLTRB(0, 30, 0, 0),
              child:  ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(colorSinbi),
                    minimumSize: MaterialStateProperty.all(Size(300, 50))
                ),
                onPressed: () {
                  _showDialog();
                },
                child: Text('방문차량 신청',
                style: TextStyle(
                  fontFamily: 'kakaoBold',
                    color: colorSinbiblue
                ),
                ),
              ),
            ),
            SizedBox(height: 20,),
            Container(alignment: Alignment.center,
              margin: EdgeInsets.fromLTRB(0, 30, 0, 0),
              child:  ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(colorSinbi),
                    minimumSize: MaterialStateProperty.all(Size(300, 50))
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => PageVisitCarList()));
                },
                child: Text('신청내역 조회',
                style: TextStyle(
                  fontFamily: 'kakaoBold',
                  color: colorSinbiblue
                ),
                ),
              ),
            ),
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: EdgeInsets.fromLTRB(5, 10, 0, 0),
                    child:  Image.asset('assets/cut1.png',width: 200,),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 0, 5, 60),
                      child: Image.asset('assets/car.jpeg', width: 150,),
                  ),
                ],
              ),
          ],
        ),
      ),
      backgroundColor: Colors.white,
    );
  }

  void _showDialog() {
    showDialog(
        context: context,
        barrierDismissible: false, // 바깥족을 누르면 닫히지 않게 학위해서 false 로준다
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("신청하려는 차량 번호를 입력해주세요",
              style: TextStyle(
                fontFamily: 'kakaoBold',
              ),
            ),
            content: Container(
              child: FormBuilder(
                key: _formKey,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                child: FormBuilderTextField(
                  name: 'carNumber',
                  decoration: InputDecoration(
                    labelText: "ex) 123가4566",
                    labelStyle: TextStyle(
                        color: colorSilver,
                      fontFamily: 'kakao',
                    ),
                    border: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                    ),
                  ),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(errorText: formErrorRequired),
                    // err 이 값은 필수입니다
                    FormBuilderValidators.maxLength(
                        8, errorText: formErrorMaxLength(8)),
                    // err $num자 이하로 입력해주세요
                    FormBuilderValidators.minLength(
                        7, errorText: formErrorMinLength(7)),
                    // err $num자 이상 입력해주세요
                  ]),
                ),
              ),
            ),
            actions: [
              Row(
                children: [
                  Image.asset('assets/sinbi3.png',width: 100),
                  Spacer(flex: 1,),
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(colorSinbi),
                    ),
                    onPressed: () {
                      if (_formKey.currentState?.saveAndValidate() ?? false) {
                        VisitCarRequest request = VisitCarRequest(
                          _formKey.currentState!.fields['carNumber']!.value,
                        );

                        Navigator.pop(
                          context,
                          [true],
                        );
                        _setVisitCar(request);
                      }
                    },
                    child: Text('신청하기',
                      style: TextStyle(
                          color:Colors.white,
                        fontFamily: 'kakaoBold',

                      ),
                    ),
                  ),
                  Spacer(flex: 1,),
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(colorSinbi),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text("닫기",
                      style: TextStyle(
                          color: Colors.white,
                        fontFamily: 'kakaoBold',
                      ),
                    ),
                  ),
                ],
              ),
            ],
          );
        }
    );
  }
}
