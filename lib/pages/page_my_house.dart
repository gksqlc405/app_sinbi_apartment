import 'package:app_sinbi_apartment/components/component_custom_loading.dart';
import 'package:app_sinbi_apartment/components/component_house_member_list.dart';
import 'package:app_sinbi_apartment/components/component_my_car_list.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/config/config_color.dart';
import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/model/admin_cost_detail.dart';
import 'package:app_sinbi_apartment/model/house_member_list_item.dart';
import 'package:app_sinbi_apartment/model/my_car_list-item.dart';
import 'package:app_sinbi_apartment/model/resident_detail.dart';
import 'package:app_sinbi_apartment/pages/page_complain_list.dart';
import 'package:app_sinbi_apartment/repository/repo_admin_cost.dart';
import 'package:app_sinbi_apartment/repository/repo_house_member.dart';
import 'package:app_sinbi_apartment/repository/repo_my_car.dart';
import 'package:app_sinbi_apartment/repository/repo_resident.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PageMyHouse extends StatefulWidget {
  const PageMyHouse({Key? key}) : super(key: key);



  @override
  State<PageMyHouse> createState() => _PageMyHouseState();
}

class _PageMyHouseState extends State<PageMyHouse> {
  final _scrollController = ScrollController();
  var price = NumberFormat('###,###,###,###');


  ResidentDetail _detail = ResidentDetail(0,'', '', '', 0, 0);
  AdminCostDetail _admin = AdminCostDetail(0, '', 0);
  List<HouseMemberListItem> _list = [];
  List<MyCarListItem> _myCar = [];
  int year = DateTime.now().year;
  int month = DateTime.now().month;

  @override
  void initState() {
    super.initState();
    _getResidentDetail();
    _houseMemberList();
    _getAdminCostDetail();
    _myCarList();

  }

  Future<void> _logout(BuildContext context) async {
    TokenLib.logout(context);
  }

  Future<void> _houseMemberList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoHouseMember().getHouseMemberList().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;

      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '실패',
      ).call();
    });
  }

  Future<void> _getResidentDetail() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoResident().getResidentDetail().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _detail = res.date;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();

      Navigator.pop(context);
    });
  }

  Future<void> _getAdminCostDetail() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoAdminCost().getAdminCostDetail(year, month).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _admin = res.date;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();

      Navigator.pop(context);
    });
  }

  Future<void> _myCarList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoMyCar().getMyCarList().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _myCar = res.list;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '실패',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        controller: _scrollController,
        children: [
          Column(crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.all(20),
                    margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                    child:
                    Text('\u{1F3E1} 현대1차아파트',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 30,
                        fontFamily: 'kakaoBold',
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    child:  IconButton(
                      onPressed: () {
                        showDialog(context: context,
                            barrierDismissible: true, //뒷배경의 touchEvent 가능여
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: const Text('로그아웃',
                                style: TextStyle(
                                  fontFamily: 'kakaoBold',
                                ),
                                ), // 제목
                                content: const Text('정말 로그아웃 하시겠습니까?',
                                style: TextStyle(
                                  fontFamily: 'kakaoBold',
                                ),
                                ), // 부제목
                                actions: [
                                  OutlinedButton(
                                      onPressed: () {
                                        _logout(context);
                                      },
                                      child: const Text('확인')
                                  ),
                                  OutlinedButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      child: const Text('취소')
                                  ),
                                ],
                              );
                            }
                        );
                      },
                      icon: Icon(Icons.logout),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 10),
                    child:  TextButton(onPressed: () {
                      showDialog(context: context,
                          barrierDismissible: true, //뒷배경의 touchEvent 가능여
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: const Text('로그아웃'), // 제목
                              content: const Text('정말 로그아웃 하시겠습니까?'), // 부제목
                              actions: [
                                OutlinedButton(
                                    onPressed: () {
                                      _logout(context);
                                    },
                                    child: const Text('확인')
                                ),
                                OutlinedButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    child: const Text('취소')
                                ),
                              ],
                            );
                          }
                      );
                    }, child: Text('로그아웃', style: TextStyle(color: colorSilver,fontFamily: 'kakaoBold',),),),
                  ),
                ],
              ),
              SizedBox(height: 50,),
              Container(
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child:
                Text('\u{1F46A} 우리집 ',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    fontFamily: 'kakaoBold',
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.fromLTRB(10, 10, 20, 10),
                margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.white,
                  border: Border.all(
                    color: colorSinbi,
                    width: 3,
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                      Text('세대주 :${_detail.residentName}',
                        style: TextStyle(
                          fontSize: 15,
                          fontFamily: 'kakaoBold',
                        ),
                      ),
                    ListView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: _list.length,
                      itemBuilder: (_, index) =>
                          ComponentHouseMemberList(item: _list[index],
                          ),
                    ),
                    SizedBox(height: 5,),
                    Text('연락처 :${_detail.phone}',
                      style: TextStyle(
                        fontSize: 15,
                        fontFamily: 'kakaoBold',
                      ),
                    ),
                    SizedBox(height: 5,),
                    Text('주소 :${_detail.address} ${_detail.aptDong}동${_detail.aptNumber}호',
                      style: TextStyle(
                        fontSize: 15,
                        fontFamily: 'kakaoBold',
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 30,),
              Container(
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child:
                Text('\u{1F4B2}관리비 ',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    fontFamily: 'kakaoBold',
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.fromLTRB(10, 10, 20, 10),
                margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.white,
                  border: Border.all(
                    color: colorSinbi,
                    width: 3,
                  ),
                ),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('${month}월 관리비 고지서',
                        style: TextStyle(
                          fontSize: 20,
                          fontFamily: 'kakaoBold',
                        ),
                      ),
                      SizedBox(height: 10,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Text('${price.format(_admin.price)}',
                                style: TextStyle(
                                  fontSize: 35,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'kakaoBold',
                                ),
                              ),
                              Container(
                                  margin: EdgeInsets.only(top: 7),
                                  child: Text('원')
                              ),
                            ],
                          ),
                          Image.asset('assets/sinbi6.png',width: 80,),
                        ],
                      ),
                    ]
                ),
              ),
              SizedBox(height: 30,),
              Container(
                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('\u{1F698} 우리집 차량 정보',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        fontFamily: 'kakaoBold',
                      ),
                    ),
                    Text('총 ${_myCar.length}대의 차량이 우리집 차량으로 등록되어있어요',
                      style: TextStyle(
                        color: colorSilver,
                        fontFamily: 'kakao',
                      ),
                    ),
                  ],
                ),
              ),
              SingleChildScrollView(
                child:
                ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: _myCar.length,
                  itemBuilder: (_, index) =>
                      ComponentMyCarList(item: _myCar[index],
                      ),
                ),
              ),
            ],
          ),
        ],

      ),
    );
  }
}
