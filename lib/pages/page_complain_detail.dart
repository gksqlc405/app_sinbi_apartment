import 'package:app_sinbi_apartment/components/component_complain_comment_list.dart';
import 'package:app_sinbi_apartment/components/component_appbar_normal.dart';
import 'package:app_sinbi_apartment/components/component_appbar_popup.dart';
import 'package:app_sinbi_apartment/components/component_custom_loading.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/config/config_color.dart';
import 'package:app_sinbi_apartment/model/complain_comments_list_item.dart';
import 'package:app_sinbi_apartment/model/complain_detail.dart';
import 'package:app_sinbi_apartment/pages/page_set_complain_comment.dart';
import 'package:app_sinbi_apartment/repository/repo_complain.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PageComplainDetail extends StatefulWidget {
  const PageComplainDetail({super.key, required this.complainId});

  final int complainId;

  @override
  State<PageComplainDetail> createState() => _PageComplainDetailState();
}

class _PageComplainDetailState extends State<PageComplainDetail> {
  String content = '';

  final _scrollController = ScrollController();
  final NumberFormat nFormat = NumberFormat('#,###');

  ComplainDetail _detail = ComplainDetail(0,'', DateTime.now(), '', '', 0);

  List<ComplainCommentsListItem> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;


  final DateFormat dateCreate = DateFormat('yyyy-MM-dd HH:mm:ss');



  @override
  void initState() {
    super.initState();
    _getComplainDetail();
    _complainCommentList();
  }

  Future<void> _complainCommentList({bool reFresh = false}) async {
    if (reFresh) {
      _list = [];
      _currentPage = 1;
      _totalPage = 1;
      _totalItemCount = 0;
    }
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoComplain().getComplainCommentList(widget.complainId).then((res) {
      BotToast.closeAllLoading();

      setState(() {

       _list = res.list;
       _totalItemCount = res.totalItemCount;

      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '실패',
      ).call();
    });

    if (reFresh) {
      _scrollController.animateTo(
          0, duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut);
    }
  }

  Future<void> _getComplainDetail() async {

    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoComplain().getComplainDetail(widget.complainId).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _detail = res.date;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();

      Navigator.pop(context);
    });


  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarNormal(
        title: '민원창구',
      ),
      body: _buildBody(),
      bottomNavigationBar: BottomAppBar(
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(colorSinbi),
            minimumSize: MaterialStateProperty.all(Size(50, 50)),
          ),
          onPressed: () async {
             await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PageSetComplainComment(
                      complainId: widget.complainId,
                      content: content,
                    )
                )
            );setState(() {
              _complainCommentList();
            });

            // if (searchResult != null && searchResult[0]) {
            //   content = searchResult[1];
            //   _complainCommentList(reFresh: true);
            //
            // }
          },
          child: Text(
            '댓글쓰기',
            style: TextStyle(
                fontFamily: 'kakaoBold',
                color: Colors.white,
              fontSize: 20
            ),
          ),
        ),
      ),
    );
  }
  Widget _buildBody() {
      return ListView(
        controller: _scrollController,
        children: [
          Center(
            child: Container(
              child: Column(crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 50,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        margin: EdgeInsets.fromLTRB(15, 0, 0, 0),
                        child:
                        Text('${      _detail.title}',
                          style: TextStyle(
                            fontFamily: 'kakaoBold',
                            fontSize: 20,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 0, 15, 0),
                        child: Text(dateCreate.format(_detail.dateCreate),
                          style: TextStyle(
                            fontSize: 9,
                            color: colorSilver,
                            fontFamily: 'kakao',
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10,),
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.fromLTRB(15, 0, 0, 0),
                        child: Text('${_detail.userName}',
                          style: TextStyle(
                              color: colorSilver,
                            fontSize: 12,
                            fontFamily: 'kakao',
                          ),
                        ),
                      ),
                      Text(' (${_detail.aptDong}동)',
                        style: TextStyle(
                            color: colorSilver,
                          fontSize: 12,
                          fontFamily: 'kakao',
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 70,),
                  Container(
                    margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    child: Text('${_detail.content}',
                      style: TextStyle(
                        fontSize: 15,
                        fontFamily: 'kakao',
                      ),
                    ),
                  ),
                  SizedBox(height: 30,),
                  Divider(thickness: 1,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.fromLTRB(15, 5, 0, 5),
                        child: Text('댓글',
                        style: TextStyle(
                          fontFamily: 'kakao',
                        ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                        child: Text("${_totalItemCount}개",
                        style: TextStyle(
                          color: colorSinbiblue,
                          fontFamily: 'kakao',
                        ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10,),
                  _buildList(),
                ],
              ),
            ),
          ),
        ],

      );

  }

  Widget _buildList() {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentComplainCommentList(item: _list[index]
            ),
          ),
        ],
      ),
    );
  }

}