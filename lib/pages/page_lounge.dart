import 'package:app_sinbi_apartment/components/component_custom_loading.dart';
import 'package:app_sinbi_apartment/components/component_house_member_list.dart';
import 'package:app_sinbi_apartment/components/component_meeting_list.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/config/config_color.dart';
import 'package:app_sinbi_apartment/model/admin_cost_detail.dart';
import 'package:app_sinbi_apartment/model/complain_title_list.dart';
import 'package:app_sinbi_apartment/model/house_member_list_item.dart';
import 'package:app_sinbi_apartment/model/meeting_list_item.dart';
import 'package:app_sinbi_apartment/model/my_car_list-item.dart';
import 'package:app_sinbi_apartment/model/resident_detail.dart';
import 'package:app_sinbi_apartment/pages/page_lounge_bulletin.dart';
import 'package:app_sinbi_apartment/repository/repo_admin_cost.dart';
import 'package:app_sinbi_apartment/repository/repo_house_member.dart';
import 'package:app_sinbi_apartment/repository/repo_meeting.dart';
import 'package:app_sinbi_apartment/repository/repo_my_car.dart';
import 'package:app_sinbi_apartment/repository/repo_resident.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PageLounge extends StatefulWidget {
  const PageLounge({Key? key}) : super(key: key);

  @override
  State<PageLounge> createState() => _PageLoungeState();
}

class _PageLoungeState extends State<PageLounge> {
  final _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _getMeetingList();
  }

  List<MeetingListItem> _list = [];


  Future<void> _getMeetingList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoMeeting().getMeetingList().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '실패',
      ).call();
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        controller: _scrollController,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                child: Text('\u{1F3E1} 현대1차아파트',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    fontFamily: 'kakaoBold',
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20, 5, 20, 0),
                child: Text('${_list.length}개의 모임이 생성되었습니다.',
                  style: TextStyle(
                    color: colorSilver,
                    fontSize: 15,
                    fontFamily: 'kakao',
                  ),
                ),
              ),
            ],
          ),
          _buildList(),
        ],
      ),
      backgroundColor: Colors.white,
    );
  }

  Widget _buildList() {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) =>
                ComponentMeetingList(item: _list[index],callback: () async {
                  final popup = await Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) =>
                          PageLoungeBulletin(
                              categoryValue: _list[index].categoryValue, category: _list[index].category,
                            image: _list[index].image, categoryTitle: _list[index].categoryTitle, categorySubTitle: _list[index].categorySubTitle,))
                  );

                  // 상세페이지에서 수정이나 삭제 완료하고 닫혔다!! 라고 알려주면 리스트 갱신하기
                  if (popup != null && popup[0]) {
                    _getMeetingList();
                  }
                },

                ),
          ),
        ],
      ),
    );
  }

}
