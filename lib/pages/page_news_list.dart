import 'package:app_sinbi_apartment/components/component_custom_loading.dart';
import 'package:app_sinbi_apartment/components/component_news_list_item.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/config/config_color.dart';
import 'package:app_sinbi_apartment/config/config_form_validator.dart';
import 'package:app_sinbi_apartment/model/news_Title_list_item.dart';
import 'package:app_sinbi_apartment/pages/page_news_detail.dart';
import 'package:app_sinbi_apartment/repository/repo_news.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageNewsList extends StatefulWidget {
  const PageNewsList({Key? key}) : super(key: key);

  @override
  State<PageNewsList> createState() => _PageNewsListState();
}

class _PageNewsListState extends State<PageNewsList> with TickerProviderStateMixin {
  final _formKey = GlobalKey<FormBuilderState>();
  late TabController _tabController;
  final _scrollController = ScrollController();

  @override
  void initState() {
    _tabController = TabController(
      length: 4,
      vsync: this,
    );
    _newsTitleList();
    _getNewsScheduleList();
    _getProgressList();
    _getCompletionList();

    super.initState();
  }

  List<NewsTitleListItem> _list = [];
  List<NewsTitleListItem> _schedule = [];
  List<NewsTitleListItem> _progress = [];
  List<NewsTitleListItem> _completion = [];

  Future<void> _newsTitleList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoNews().getNewsTitleList().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '실패',
      ).call();
    });
  }

  Future<void> _getNewsScheduleList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoNews().getNewsScheduleList().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _schedule = res.list;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '실패',
      ).call();
    });
  }

  Future<void> _getProgressList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoNews().getProgressList().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _progress = res.list;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '실패',
      ).call();
    });
  }

  Future<void> _getCompletionList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoNews().getCompletionList().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _completion = res.list;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '실패',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            width: 300,
            padding: EdgeInsets.all(30),
            margin: EdgeInsets.fromLTRB(0, 40, 0, 10),
            decoration: BoxDecoration(
              color: colorGumbi,
              boxShadow: [
                BoxShadow(
                  color: colorGumbi,
                  blurRadius: 5.0,
                  spreadRadius: 1.0,
                ),
              ],
              border: Border.all(
                color: colorGumbi,
                style: BorderStyle.solid,
                width: 5,
              ),
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text('아파트소식',
                      style: TextStyle(
                        fontSize: 35,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontFamily: 'kakaoBold',
                      ),
                    ),
                    Spacer(flex: 1,),
                    Image.asset('assets/gumbi1.png', height: 80, width: 70,),
                  ],
                ),

                Text('아파트소식 게시판입니다.\n여러 아파트 소식들을 접해보세요!!.',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'kakao',
                  ),
                ),
                SizedBox(height: 10,),
                Text('(관리사무소에서 운영되는 곳이에요.)',
                style: TextStyle(
                  fontSize: 13,
                  color: colorSilver,
                  fontFamily: 'kakao',
                ),
                ),
              ],
            ),
          ),
          Container(
            child: TabBar(
              indicatorColor: colorGumbi,
              // 탭바 밑줄 색상
              tabs: [
                Container(
                  height: 40,
                  alignment: Alignment.center,
                  child: Text(
                    '모든민원',
                    style: TextStyle(
                        color: Colors.black,
                      fontFamily: 'kakao',
                    ),
                  ),
                ),
                Container(
                  height: 40,
                  alignment: Alignment.center,
                  child: Text(
                    '예정',
                    style: TextStyle(
                        color: Colors.black,
                      fontFamily: 'kakao',
                    ),
                  ),
                ),
                Container(
                  height: 40,
                  alignment: Alignment.center,
                  child: Text(
                    '진행',
                    style: TextStyle(
                        color: Colors.black,
                      fontFamily: 'kakao',
                    ),
                  ),
                ),
                Container(
                  height: 40,
                  alignment: Alignment.center,
                  child: Text(
                    '완료',
                    style: TextStyle(
                        color: Colors.black,
                      fontFamily: 'kakao',
                    ),
                  ),
                ),
              ],
              labelColor: Colors.black,
              unselectedLabelColor: Colors.black,
              controller: _tabController,
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(150, 10, 0, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
              ],
            ),
          ),

          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: [
                Container(
                  alignment: Alignment.center,
                  child: ListView(
                    controller: _scrollController,
                    children: [
                      _buildList(),
                    ],
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  child: ListView(
                    controller: _scrollController,
                    children: [
                      _registerList(),
                    ],
                  ),
                ),
                Container(
                    alignment: Alignment.center,
                    child: ListView(
                      controller: _scrollController,
                      children: [
                        _progressList()
                      ],
                    )
                ),
                Container(
                    alignment: Alignment.center,
                    child: ListView(
                      controller: _scrollController,
                      children: [
                        _completionList()
                      ],
                    )
                ),
              ],
            ),
          ),

        ],
      ),
      backgroundColor: colorGumbiPingk,
    );
  }


  Widget _buildList() {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) =>
                ComponentNewsListItem(item: _list[index], callback: () async {
                  final popup = await Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) =>
                          PageNewsDetail(
                              newsId: _list[index].newsId))
                  );

                  // 상세페이지에서 수정이나 삭제 완료하고 닫혔다!! 라고 알려주면 리스트 갱신하기
                  if (popup != null && popup[0]) {
                    _newsTitleList();
                  }
                },

                ),
          ),
        ],
      ),
    );
  }

  Widget _registerList() {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _schedule.length,
            itemBuilder: (_, index) =>
                ComponentNewsListItem(
                  item: _schedule[index], callback: () async {
                  final popup = await Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) =>
                          PageNewsDetail(
                              newsId: _schedule[index].newsId))
                  );

                  // 상세페이지에서 수정이나 삭제 완료하고 닫혔다!! 라고 알려주면 리스트 갱신하기
                  if (popup != null && popup[0]) {
                    _newsTitleList();
                  }
                },

                ),
          ),
        ],
      ),
    );
  }

  Widget _progressList() {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _progress.length,
            itemBuilder: (_, index) =>
                ComponentNewsListItem(
                  item: _progress[index], callback: () async {
                  final popup = await Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) =>
                          PageNewsDetail(
                              newsId: _progress[index].newsId))
                  );

                  // 상세페이지에서 수정이나 삭제 완료하고 닫혔다!! 라고 알려주면 리스트 갱신하기
                  if (popup != null && popup[0]) {
                    _newsTitleList();
                  }
                },

                ),
          ),
        ],
      ),
    );
  }

  Widget _completionList() {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _completion.length,
            itemBuilder: (_, index) =>
                ComponentNewsListItem(
                  item: _completion[index], callback: () async {
                  final popup = await Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) =>
                          PageNewsDetail(
                              newsId: _completion[index].newsId))
                  );

                  // 상세페이지에서 수정이나 삭제 완료하고 닫혔다!! 라고 알려주면 리스트 갱신하기
                  if (popup != null && popup[0]) {
                    _newsTitleList();
                  }
                },

                ),
          ),
        ],
      ),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView( // column 등 이것저것 넣으면 키보드가 활성화 될때 나올때 에러가 나올수있어 넣어준다
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Container(
          padding: EdgeInsets.all(5),
          margin: EdgeInsets.all(5),
          child: Column(
            children: [
              SizedBox(height: 20,),
              FormBuilderTextField(
                name: 'title',
                decoration: const InputDecoration(

                  labelText: '제목',
                  labelStyle: TextStyle(
                  ),
                  suffixIcon: Text(
                    '2자이상~30자이하   ',
                    style: TextStyle(
                      wordSpacing: 5,
                      height: 5,
                      fontSize: 10,
                      color: colorMain,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(60)),
                    borderSide: BorderSide(color: colorMain),
                  ),
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  // err 이 값은 필수입니다
                  FormBuilderValidators.minLength(
                      2, errorText: formErrorMinLength(2)),
                  // err $num자 이상 입력해주세요
                  FormBuilderValidators.maxLength(
                      30, errorText: formErrorMaxLength(30)),
                  // err $num자 이하로 입력해주세요
                ]),
                keyboardType: TextInputType.text,
              ),
              FormBuilderTextField(
                name: 'content',
                decoration: const InputDecoration(

                  labelText: '내용',
                  labelStyle: TextStyle(
                  ),
                  suffixIcon: Text(
                    '100자 이내   ',
                    style: TextStyle(
                      wordSpacing: 5,
                      height: 5,
                      fontSize: 10,
                      color: colorMain,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(60)),
                    borderSide: BorderSide(color: colorMain),
                  ),
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  // err 이 값은 필수입니다
                  FormBuilderValidators.maxLength(
                      100, errorText: formErrorMaxLength(100)),
                  // err $num자 이하로 입력해주세요
                ]),
                keyboardType: TextInputType.text,
              ),
            ],
          ),
        ),

      ),
    );
  }
}