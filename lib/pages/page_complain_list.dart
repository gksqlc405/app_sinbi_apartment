import 'package:app_sinbi_apartment/components/component_appbar_normal.dart';
import 'package:app_sinbi_apartment/components/component_custom_loading.dart';
import 'package:app_sinbi_apartment/components/component_list_item.dart';
import 'package:app_sinbi_apartment/components/component_no_contents.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/components/component_register_item.dart';
import 'package:app_sinbi_apartment/config/config_color.dart';
import 'package:app_sinbi_apartment/config/config_form_validator.dart';
import 'package:app_sinbi_apartment/model/complain_comments_list_item.dart';
import 'package:app_sinbi_apartment/model/complain_comments_list_result.dart';
import 'package:app_sinbi_apartment/model/complain_request.dart';
import 'package:app_sinbi_apartment/model/complain_state_list_item.dart';
import 'package:app_sinbi_apartment/model/complain_title_list.dart';
import 'package:app_sinbi_apartment/pages/Page_set_complain.dart';
import 'package:app_sinbi_apartment/pages/page_complain_detail.dart';
import 'package:app_sinbi_apartment/repository/repo_complain.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';

class PageComplainList extends StatefulWidget {
  const PageComplainList({super.key, required this.complainId});


  final int complainId;


  @override
  State<PageComplainList> createState() => _PageComplainListState();
}

class _PageComplainListState extends State<PageComplainList>with TickerProviderStateMixin {
  final _formKey = GlobalKey<FormBuilderState>();
  late TabController _tabController;
  final _scrollController = ScrollController();
  
  List<ComplainCommentsListItem> comment = [];





  @override
  void initState() {
    _tabController = TabController(
      length: 4,
      vsync: this, //vsync에 this 형태로 전달해야 애니메이션이 정상 처리됨
    );
    _complainList();
    _getRegisterList();
    _getProgressList();
    _getCompletion();




    super.initState();
  }

  List<ComplainTitleList> _list = [];
  List<ComplainStateListItem> _register = [];
  List<ComplainStateListItem> _progress = [];
  List<ComplainStateListItem> _completion = [];


  Future<void> _complainList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoComplain().getList().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '실패',
      ).call();
    });
  }


  Future<void> _getRegisterList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoComplain().getRegisterList().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _register = res.list;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '실패',
      ).call();
    });
  }

  Future<void> _getProgressList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoComplain().getProgressList().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _progress = res.list;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '실패',
      ).call();
    });
  }

  Future<void> _getCompletion() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoComplain().getCompletionList().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _completion = res.list;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '실패',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            width: 300,
            padding: EdgeInsets.all(30),
            margin: EdgeInsets.fromLTRB(0, 40, 0, 10),
            decoration: BoxDecoration(
              color: colorSinbi,
              boxShadow: [
                BoxShadow(
                  color: colorSinbi,
                  blurRadius: 5.0,
                  spreadRadius: 1.0,
                ),
              ],
              border: Border.all(
                color: colorSinbi,
                style: BorderStyle.solid,
                width: 5,
              ),
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text('민원창구',
                      style: TextStyle(
                        fontFamily: 'kakaoBold',
                        fontSize: 35,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                    Spacer(flex: 1,),
                    Image.asset('assets/sinbi1.png', height: 95, width: 100,),
                  ],
                ),

                Text('살면서 아쉽고 불편한 사항들에 대해\n관리사무소와 임대인에게 알려주세요.',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'kakao',
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: TabBar(
              indicatorColor: colorSinbi,
              // 탭바 밑줄 색상
              tabs: [
                Container(
                  height: 40,
                  alignment: Alignment.center,
                  child: Text(
                    '모든민원',
                    style: TextStyle(
                        color: Colors.white,
                      fontFamily: 'kakao',
                    ),
                  ),
                ),
                Container(
                  height: 40,
                  alignment: Alignment.center,
                  child: Text(
                    '접수',
                    style: TextStyle(
                        color: Colors.white,
                      fontFamily: 'kakao',
                    ),
                  ),
                ),
                Container(
                  height: 40,
                  alignment: Alignment.center,
                  child: Text(
                    '진행',
                    style: TextStyle(
                        color: Colors.white,
                      fontFamily: 'kakao',
                    ),
                  ),
                ),
                Container(
                  height: 40,
                  alignment: Alignment.center,
                  child: Text(
                    '완료',
                    style: TextStyle(
                        color: Colors.white,
                      fontFamily: 'kakao',
                    ),
                  ),
                ),
              ],
              labelColor: Colors.black,
              unselectedLabelColor: Colors.black,
              controller: _tabController,
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(150, 10, 0, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TextButton(onPressed: () async {
                  await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          PageSetComplain(
                          ),
                    ),
                  );
                  setState(() {
                    _complainList();
                    _getProgressList();
                    _getCompletion();
                    _getRegisterList();
                  });
                }, child: Text('<글쓰기> \u{270F}',
                  style: TextStyle(
                      color: Colors.white,
                    fontFamily: 'kakao',
                  ),
                ),
                ),
              ],
            ),
          ),

          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: [
                Container(
                  alignment: Alignment.center,
                  child: ListView(
                    controller: _scrollController,
                    children: [
                      _buildList(),
                    ],
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  child: ListView(
                    controller: _scrollController,
                    children: [
                      _registerList(),
                    ],
                  ),
                ),
                Container(
                    alignment: Alignment.center,
                    child: ListView(
                      controller: _scrollController,
                      children: [
                        _progressList()
                      ],
                    )
                ),
                Container(
                    alignment: Alignment.center,
                    child: ListView(
                      controller: _scrollController,
                      children: [
                        _completionList()
                      ],
                    )
                ),
              ],
            ),
          ),

        ],
      ),
      backgroundColor: colorSinbiblue,
    );
  }


  Widget _buildList() {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) =>
                ComponentListItem(item: _list[index], callback: () async {
                  final popup = await Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) =>
                          PageComplainDetail(
                              complainId: _list[index].complainId))
                  );

                  // 상세페이지에서 수정이나 삭제 완료하고 닫혔다!! 라고 알려주면 리스트 갱신하기
                  if (popup != null && popup[0]) {
                    _complainList();
                  }
                },

                ),
          ),
        ],
      ),
    );
  }

  Widget _registerList() {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _register.length,
            itemBuilder: (_, index) =>
                ComponentRegisterItem(
                  item: _register[index], callback: () async {
                  final popup = await Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) =>
                          PageComplainDetail(
                              complainId: _register[index].complainId))
                  );

                  // 상세페이지에서 수정이나 삭제 완료하고 닫혔다!! 라고 알려주면 리스트 갱신하기
                  if (popup != null && popup[0]) {
                    _complainList();
                  }
                },

                ),
          ),
        ],
      ),
    );
  }

  Widget _progressList() {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _progress.length,
            itemBuilder: (_, index) =>
                ComponentRegisterItem(
                  item: _progress[index], callback: () async {
                  final popup = await Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) =>
                          PageComplainDetail(
                              complainId: _progress[index].complainId))
                  );

                  // 상세페이지에서 수정이나 삭제 완료하고 닫혔다!! 라고 알려주면 리스트 갱신하기
                  if (popup != null && popup[0]) {
                    _complainList();
                  }
                },

                ),
          ),
        ],
      ),
    );
  }

  Widget _completionList() {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _completion.length,
            itemBuilder: (_, index) =>
                ComponentRegisterItem(
                  item: _completion[index], callback: () async {
                  final popup = await Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) =>
                          PageComplainDetail(
                              complainId: _completion[index].complainId))
                  );

                  // 상세페이지에서 수정이나 삭제 완료하고 닫혔다!! 라고 알려주면 리스트 갱신하기
                  if (popup != null && popup[0]) {
                    _complainList();
                  }
                },

                ),
          ),
        ],
      ),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView( // column 등 이것저것 넣으면 키보드가 활성화 될때 나올때 에러가 나올수있어 넣어준다
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Container(
          padding: EdgeInsets.all(5),
          margin: EdgeInsets.all(5),
          child: Column(
            children: [
              SizedBox(height: 20,),
              FormBuilderTextField(
                name: 'title',
                decoration: const InputDecoration(

                  labelText: '제목',
                  labelStyle: TextStyle(
                  ),
                  suffixIcon: Text(
                    '2자이상~30자이하   ',
                    style: TextStyle(
                      wordSpacing: 5,
                      height: 5,
                      fontSize: 10,
                      color: colorMain,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(60)),
                    borderSide: BorderSide(color: colorMain),
                  ),
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  // err 이 값은 필수입니다
                  FormBuilderValidators.minLength(
                      2, errorText: formErrorMinLength(2)),
                  // err $num자 이상 입력해주세요
                  FormBuilderValidators.maxLength(
                      30, errorText: formErrorMaxLength(30)),
                  // err $num자 이하로 입력해주세요
                ]),
                keyboardType: TextInputType.text,
              ),
              FormBuilderTextField(
                name: 'content',
                decoration: const InputDecoration(

                  labelText: '내용',
                  labelStyle: TextStyle(
                  ),
                  suffixIcon: Text(
                    '100자 이내   ',
                    style: TextStyle(
                      wordSpacing: 5,
                      height: 5,
                      fontSize: 10,
                      color: colorMain,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(60)),
                    borderSide: BorderSide(color: colorMain),
                  ),
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  // err 이 값은 필수입니다
                  FormBuilderValidators.maxLength(
                      100, errorText: formErrorMaxLength(100)),
                  // err $num자 이하로 입력해주세요
                ]),
                keyboardType: TextInputType.text,
              ),
            ],
          ),
        ),

      ),
    );
  }
}





