import 'package:app_sinbi_apartment/components/component_appbar_normal.dart';
import 'package:app_sinbi_apartment/components/component_custom_loading.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/components/component_visit_car_list.dart';
import 'package:app_sinbi_apartment/model/visit_car_list_item.dart';
import 'package:app_sinbi_apartment/repository/repo_visit_car.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageVisitCarList extends StatefulWidget {
  const PageVisitCarList({Key? key}) : super(key: key);



  @override
  State<PageVisitCarList> createState() => _PageVisitCarListState();
}

class _PageVisitCarListState extends State<PageVisitCarList> {
  final _scrollController = ScrollController();




  List<VisitCarListItem> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;

  void initState() {
    super.initState();
    _visitCarList();
  }

  Future<void> _visitCarList({bool reFresh = false}) async {
    if (reFresh) {
      _list = [];
      _currentPage = 1;
      _totalPage = 1;
      _totalItemCount = 0;
    }

    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoVisitCar().getVisitCarList().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '실패',
      ).call();
    });

    if (reFresh) {
      _scrollController.animateTo(
          0, duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut);
    }
  }

  Future<void> _delVisitCar(int visitCarId) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoVisitCar().delVisitCar(visitCarId).then((res) {
      BotToast.closeAllLoading();

      Navigator.pop(
        context,
        [true,],
      );


    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '실패',
      ).call();
    });
  }

  void _showDelDialog(int visitCarId) {
    showDialog(context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('정말 취소하시겠습니까??',
            style: TextStyle(
              fontFamily: 'kakaoBold',
            ),
            ),
            content: const Text('한번 취소하시면 되돌릴수 없습니다.',
            style: TextStyle(
              fontFamily: 'kakaoBold',
            ),
            ),
            actions: [
              OutlinedButton(
                  onPressed: () {
                    _delVisitCar(visitCarId);
                    _visitCarList(reFresh: true);
                  },
                  child: const Text('확인')
              ),
              OutlinedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('취소')
              ),
            ],
          );
        }
    );
  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: ComponentAppbarNormal(
        title: '방문차량 신청내역 조회',
      ),
      body: ListView(
        controller: _scrollController,
        children: [
          _listBody(),
        ],
      ),
    );
  }

  Widget _listBody() {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _list.length,
              itemBuilder: (_, index) =>
          ComponentVisitCarList(item: _list[index], callback: () async {
            _showDelDialog(_list[index].visitCarId);
            },
          ),
          ),
        ],
      ),
    );
  }
}
