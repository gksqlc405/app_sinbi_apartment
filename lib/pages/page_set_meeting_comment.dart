import 'package:app_sinbi_apartment/components/component_appbar_normal.dart';
import 'package:app_sinbi_apartment/components/component_custom_loading.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/config/config_color.dart';
import 'package:app_sinbi_apartment/config/config_form_validator.dart';
import 'package:app_sinbi_apartment/model/meeting_comment_request.dart';
import 'package:app_sinbi_apartment/repository/repo_meeting.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageSetMeetingComment extends StatefulWidget {
  const PageSetMeetingComment({super.key, required this.meetingId});

  final int meetingId;


  @override
  State<PageSetMeetingComment> createState() => _PageSetMeetingCommentState();
}

class _PageSetMeetingCommentState extends State<PageSetMeetingComment> {
  final _formKey = GlobalKey<FormBuilderState>();




  Future<void> _setMeetingComment(int meetingId , MeetingCommentRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoMeeting().setMeetingComment(meetingId, request).then((res)  {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '댓글 작성이 완료 되었습니다.',
        subTitle: '완료',
      ).call();

      Navigator.pop(
          context,
          [true]

      );

    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false, title: '댓글작성이 실패하였습니다', subTitle: '양식에 맞는지 확인해주십시오',
      ).call();
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarNormal(
        title: '댓글쓰기',
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Container(
          padding: EdgeInsets.all(5),
          margin: EdgeInsets.all(5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 30,),
              Container(
                margin: EdgeInsets.only(bottom: 15),
                child: Text("댓글",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              FormBuilderTextField(
                maxLines: 10,
                maxLength: 100,
                name: 'content',
                decoration: InputDecoration(
                  labelText: '댓글을 입력해주세요.',
                  suffixIcon: Text('2글자 이상 입력해주세요',
                    style: TextStyle(
                      color: colorSilver,
                      fontSize: 10,
                    ),
                  ),
                  labelStyle: TextStyle(
                    fontSize: 20,
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: colorSilver),
                  ),
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  // err 이 값은 필수입니다
                  FormBuilderValidators.minLength(
                      2, errorText: formErrorMinLength(2)
                  ),
                  FormBuilderValidators.maxLength(
                      100, errorText: formErrorMaxLength(100)),
                  // err $num자 이하로 입력해주세요
                ]),
                keyboardType: TextInputType.text,
              ),
              SizedBox(height: 30,),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 15),
                    child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(colorSilver),
                        ),
                        onPressed: () {
                          _showSetDialog();
                        }, child: Text('입력하기')),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _showSetDialog() {
    showDialog(context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('댓글작성을 완료 하시겠습니까?'), // 제목
            actions: [
              OutlinedButton(
                  onPressed: () {
                    if (_formKey.currentState?.saveAndValidate() ?? false) {
                      MeetingCommentRequest request = MeetingCommentRequest(
                        _formKey.currentState!.fields['content']!.value,
                      );

                      Navigator.pop(
                        context,
                        [true],
                      );
                      _setMeetingComment(widget.meetingId, request);
                    }
                  },
                  child: const Text('확인')
              ),
              OutlinedButton(
                  onPressed: () {
                    Navigator.pop(context);  // 취소일땐 삭제하면 안돼기 때문에 pop
                  },
                  child: const Text('취소')
              ),
            ],
          );
        }
    );
  }
}
