import 'package:app_sinbi_apartment/components/component_custom_loading.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/config/config_color.dart';
import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/model/complain_comments_list_result.dart';
import 'package:app_sinbi_apartment/model/resident_detail.dart';
import 'package:app_sinbi_apartment/pages/page_complain_list.dart';
import 'package:app_sinbi_apartment/pages/page_news_list.dart';
import 'package:app_sinbi_apartment/repository/repo_resident.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageWeApartment extends StatefulWidget {
   const PageWeApartment({Key? key}) : super(key: key);



  @override
  State<PageWeApartment> createState() => _PageWeApartmentState();
}

class _PageWeApartmentState extends State<PageWeApartment> {
  ResidentDetail _detail = ResidentDetail(0, '', '', '', 0, 0);
  int complainId = 0;


  @override
  void initState() {
    super.initState();
    _getResidentDetail();
  }

  Future<void> _getResidentDetail() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    await RepoResident().getResidentDetail().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _detail = res.date;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();

      Navigator.pop(context);
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
             Column(
               crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
                    margin: EdgeInsets.fromLTRB(0, 20, 100, 0),
                    child:
                    Text('\u{1F3E1} 현대1차아파트',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 25,
                        fontFamily: 'kakaoBold',
                      ),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                    child:    Text('환영합니다  ${_detail.residentName} 입주자님', style: TextStyle(color: colorSilver,fontFamily: 'kakao',),)
                  ),
                ],
              ),
            SizedBox(height: 50,),
            Container(
              padding: EdgeInsets.all(10),
              margin: EdgeInsets.all(10),
              child: Image.asset('assets/title.png',width: 280, height: 150),
            ),
            Container(
              decoration: BoxDecoration(
                color:colorSinbi,
                boxShadow: [
                  BoxShadow(
                    color: colorSinbi,
                    blurRadius: 5.0,
                    spreadRadius: 1.0,
                  ),
                ],
                border: Border.all(
                  color:colorSinbi,
                  style: BorderStyle.solid,
                  width: 5,
                ),
                borderRadius: BorderRadius.circular(20.0),
              ),
              padding: EdgeInsets.all(45),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('민원창구',
                  style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontFamily: 'kakaoBold',
                  ),
                  ),
                  SizedBox(height: 30,),
                  Text('민원창구 게시판입니다.\n아파트 주민만 이용할수있습니다.',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'kakao',
                  ),
                  ),
                  SizedBox(height: 10,),
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(colorSinbiblue),
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageComplainList(complainId: complainId))
                      );
                    },
                    child: Text('입장하기',
                    style: TextStyle(
                      fontFamily: 'kakaoBold',
                    ),),
                  ),
                ],
              ),
            ),
            SizedBox(height: 30,),
            Row(mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [

                Image.asset('assets/humbi2.png', width: 300,),
              ],
            ),
            SizedBox(height: 30,),
            Container(
              decoration: BoxDecoration(
                color:colorGumbi,
                boxShadow: [
                  BoxShadow(
                    color: colorGumbi,
                    blurRadius: 5.0,
                    spreadRadius: 1.0,
                  ),
                ],
                border: Border.all(
                  color:colorGumbi,
                  style: BorderStyle.solid,
                  width: 5,
                ),
                borderRadius: BorderRadius.circular(20.0),
              ),
              padding: EdgeInsets.all(50),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('아파트 소식',
                    style: TextStyle(
                      fontSize: 35,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontFamily: 'kakaoBold',
                    ),
                  ),
                  SizedBox(height: 30,),
                  Text('우리 아파트의 다양한 소식들을\n놓치지말고 확인하세요!',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'kakao',
                    ),
                  ),
                  SizedBox(height: 10,),
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(colorGumbiPingk),
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageNewsList()));
                    },
                    child: Text('입장하기',
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'kakaoBold',
                    ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      backgroundColor: Colors.white,
    );
  }


}
