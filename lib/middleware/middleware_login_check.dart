import 'package:app_sinbi_apartment/functions/token_lib.dart';
import 'package:app_sinbi_apartment/pages/page_main.dart';
import 'package:app_sinbi_apartment/pages/page_resident_login.dart';
import 'package:flutter/material.dart';

class MiddlewareLoginCheck {
  void check(BuildContext context) async {
    String? token = await TokenLib.getToken();

    if(token == null || token == '') {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageResidentLogin()), (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => PageMain()), (route) => false);
    }
  }
}