
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/pages/page_resident_login.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TokenLib {
  static Future<String?> getToken() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getString('token');
  }

  static void setToken(String token) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('token', token);
  }

  static void logout(BuildContext context) async {
    BotToast.closeAllLoading();

    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('token', '');

    ComponentNotification(
        success: false,
        title: '로그아웃',
        subTitle: '로그아웃되어 로그인 화면으로 이동합니다.'
    ).call();
    
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext cotext) => const PageResidentLogin()), (route) => false);
  }
}