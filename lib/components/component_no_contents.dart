import 'package:flutter/material.dart';

class ComponentNoContents extends StatelessWidget {
  const ComponentNoContents({super.key, required this.imageName, required this.msg});

  // final IconData icon;
  final String imageName;
  final String msg;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:  Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(height: 15,),
            Text(msg,
              style: TextStyle(
                fontFamily: 'naverTitle',
                fontSize: 15,
              ),
            ),
          ],
        ),
      ),
      backgroundColor: Colors.white,
    );

  }
}
