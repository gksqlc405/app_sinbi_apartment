import 'package:app_sinbi_apartment/model/complain_state_list_item.dart';
import 'package:flutter/material.dart';

class ComponentRegisterItem extends StatelessWidget {
  const ComponentRegisterItem({super.key, required this.item, required this.callback});


  final ComplainStateListItem item;
  final VoidCallback callback;




  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        padding: const EdgeInsets.all(25),
        margin: EdgeInsets.all(7),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          border: Border.all(color: Color.fromARGB(50, 20, 20, 0)),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(item.title,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
                fontFamily: 'kakaoBold',
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text('<${item.serviceState}>',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 13,
                color: Colors.black,
                fontFamily: 'kakao',

              ),
            ),
          ],
        ),
      ),
    );
  }
}

