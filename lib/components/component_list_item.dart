
import 'package:app_sinbi_apartment/model/complain_comments_list_result.dart';
import 'package:app_sinbi_apartment/model/complain_title_list.dart';
import 'package:app_sinbi_apartment/pages/page_complain_detail.dart';
import 'package:flutter/material.dart';

class ComponentListItem extends StatelessWidget {
  const ComponentListItem({super.key, required this.item, required this.callback});

  final ComplainTitleList item;
  final VoidCallback callback;





  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        padding: const EdgeInsets.all(25),
        margin: EdgeInsets.all(7),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
          border: Border.all(
            color: Colors.white,
            width: 3,
          ),
        ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('${item.title} ',
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
                fontFamily: 'kakaoBold',
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text('<${item.serviceState}>',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 13,
                color: Colors.black,
                fontFamily: 'kakao',

              ),
            ),
          ],
        ),
      ),
    );
  }
}

