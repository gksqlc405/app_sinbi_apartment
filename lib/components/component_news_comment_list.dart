import 'package:app_sinbi_apartment/config/config_color.dart';
import 'package:app_sinbi_apartment/model/news_comment_list_item.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ComponentNewsCommentList extends StatelessWidget {
  const ComponentNewsCommentList({super.key, required this.item});

  final NewsCommentListItem item;

  @override
  Widget build(BuildContext context) {
    final DateFormat dateCreate = DateFormat('yyyy-MM-dd HH:mm:ss');

    return GestureDetector(
      child: Container(
        padding: const EdgeInsets.fromLTRB(0, 13, 0, 13),
        margin: EdgeInsets.only(bottom: 5),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Color.fromARGB(30, 20, 20, 0)),
        ),
        child: Column(
          children: [
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: 180,
                  margin: EdgeInsets.fromLTRB(15, 0, 0, 0),
                  child: Text(item.content,
                    style: TextStyle(
                      fontSize: 15,
                      color: Colors.black,
                      fontFamily: 'kakao',
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 15, 0),
                  child: Text(dateCreate.format(item.dateCreate),
                    style: TextStyle(
                      fontSize: 8,
                      color: colorSilver,
                      fontFamily: 'kakao',
                    ),
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(15, 0, 0, 0),
                  child: Text(item.username,
                    style: TextStyle(
                      fontSize: 9,
                      color: colorSilver,
                      fontFamily: 'kakao',
                    ),
                  ),
                ),
                Text('(${item.residentGroup})',
                  style: TextStyle(
                    fontSize: 9,
                    color: colorSilver,
                    fontFamily: 'kakao',
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
