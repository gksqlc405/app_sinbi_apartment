import 'package:app_sinbi_apartment/model/news_Title_list_item.dart';
import 'package:flutter/material.dart';

class ComponentNewsListItem extends StatelessWidget {
  const ComponentNewsListItem({super.key, required this.item, required this.callback});


  final NewsTitleListItem item;
  final VoidCallback callback;




  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        padding: const EdgeInsets.all(25),
        // margin: EdgeInsets.only(bottom: 5),
        margin: EdgeInsets.all(7),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          border: Border.all(
            color: Colors.white,
            width: 3,
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(item.title,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
                fontFamily: 'kakaoBold',
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text('<${item.newsState}>',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 13,
                color: Colors.black,
                fontFamily: 'kakao',

              ),
            ),
          ],
        ),
      ),
    );
  }
}

