
import 'package:flutter/material.dart';

class ComponentAppbarFilter extends StatefulWidget  implements PreferredSizeWidget {   // 사이즈를 조절하는것을 구현한다
  const ComponentAppbarFilter({super.key, required this.title, required this.actionIcon, required this.callback});

  final String title;
  final IconData actionIcon;
  final VoidCallback callback;

  @override
  State<ComponentAppbarFilter> createState() => _ComponentAppbarFilterState();

  @override
  Size get preferredSize {
    return const Size.fromHeight(40);
  }

}

class _ComponentAppbarFilterState extends State<ComponentAppbarFilter> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      centerTitle: true,   // 타이틀 글자를 센터로 하겠니
      automaticallyImplyLeading: false,   // 타이틀 옆에 자동으로 생성되는 뒤로가기버튼 허용하겠니?
      title: Text(widget.title,
        style: TextStyle(
            fontFamily: 'naverTitle',

        ),
      ),
      elevation: 5, // 앱바 아래 그림자처럼 보이는것
      actions: [
        IconButton(onPressed: widget.callback, icon: Icon(widget.actionIcon)) // onPressed에 () {}가 있다는것은 내가 처리한다는거기때문에  콜백으로 처리
      ],
    );
  }
}
