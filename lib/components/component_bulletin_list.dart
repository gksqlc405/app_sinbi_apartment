import 'package:app_sinbi_apartment/model/meeting_bulletin_list_item.dart';
import 'package:flutter/material.dart';

class ComponentBulletinList extends StatelessWidget {
  const ComponentBulletinList({super.key, required this.item, required this.callback});

  final MeetingBulletinListItem item;
  final VoidCallback callback;





  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        padding: const EdgeInsets.all(25),
        margin: EdgeInsets.all(7),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
          border: Border.all(
            color: Colors.white,
            width: 3,
          ),
        ),
        child: Row(
         mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(item.title,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
                fontFamily: 'kakaoBold',
              ),
            ),
            Icon(Icons.keyboard_arrow_right_outlined)
          ],
        ),
      ),
    );
  }
}

