import 'package:app_sinbi_apartment/components/component_custom_loading.dart';
import 'package:app_sinbi_apartment/components/component_notification.dart';
import 'package:app_sinbi_apartment/config/config_color.dart';
import 'package:app_sinbi_apartment/model/visit_car_list_item.dart';
import 'package:app_sinbi_apartment/pages/page_visit_car_list.dart';
import 'package:app_sinbi_apartment/repository/repo_visit_car.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ComponentVisitCarList extends StatelessWidget {
  const ComponentVisitCarList({super.key, required this.item, required this.callback});

  final VisitCarListItem item;
  final VoidCallback callback;




  @override
  Widget build(BuildContext context) {
    final DateFormat dateCreate = DateFormat('yyyy-MM-dd HH:mm:ss');
      return GestureDetector(
        child: Container(
          padding: const EdgeInsets.all(20),
          margin: EdgeInsets.all(7),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
            border: Border.all(color: Color.fromARGB(50, 20, 20, 0)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(item.carNumber,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontFamily: 'kakaoBold',
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 5),
                    child:  Text('(${item.approvalStatus})',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 13,
                        color: colorSilver,
                        fontFamily: 'kakao',
                      ),
                    ),
                  ),
                  if (item.approvalStatus != '처리완료')
                    Container(
                      margin: EdgeInsets.only(left: 80),
                      child: TextButton(onPressed: callback, child: Text('취소하기',
                        style: TextStyle(
                          color: Colors.red,
                          fontFamily: 'kakaoBold',
                        ),
                      ),

                      ),
                    ),
                ],
              ),
              SizedBox(height: 10,),
              Text('등록날짜 : ${dateCreate.format(item.dateCreate)}',
                style: TextStyle(
                  color: colorSilver,
                  fontFamily: 'kakao',
                ),
              ),
            ],
          ),
        )
      );
  }
}

