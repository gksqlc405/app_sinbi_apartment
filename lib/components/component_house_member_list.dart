import 'package:app_sinbi_apartment/model/house_member_list_item.dart';
import 'package:flutter/material.dart';

class ComponentHouseMemberList extends StatelessWidget {
  const ComponentHouseMemberList({super.key, required this.item});

  final HouseMemberListItem item;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        child: Row(
          children: [
            Text('세대원 : ${item.name}',
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
                fontFamily: 'kakaoBold',
              ),
            ),
          ],),
    );
  }
}

