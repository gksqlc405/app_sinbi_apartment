import 'package:app_sinbi_apartment/config/config_color.dart';
import 'package:app_sinbi_apartment/model/meeting_list_item.dart';
import 'package:flutter/material.dart';

class ComponentMeetingList extends StatelessWidget {
  const ComponentMeetingList({super.key, required this.item, required this.callback});

  final MeetingListItem item;
  final VoidCallback callback;




  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        padding: const EdgeInsets.all(8),
        margin: EdgeInsets.fromLTRB(20, 30, 20, 20),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(242, 241, 240, 10),
              blurRadius: 20.0,
              spreadRadius: 1.0,
            ),
          ],
          borderRadius: BorderRadius.circular(30),
          color: Colors.white,
          border: Border.all(
            color: Colors.black,
            width: 1,
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset('assets/${item.image}',),
            SizedBox(height: 10,),
            Text(item.category,
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.black,
                fontFamily: 'kakaoBold',
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(item.categoryTitle,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15,
                color: colorSilver,
                fontFamily: 'kakao',
              ),
            ),
            Text(item.categorySubTitle,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15,
                color: colorSilver,
                fontFamily: 'kakao',
              ),
            ),

          ],
        ),
      ),
    );
  }
}

