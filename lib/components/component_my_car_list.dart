import 'package:app_sinbi_apartment/config/config_color.dart';
import 'package:app_sinbi_apartment/model/my_car_list-item.dart';
import 'package:flutter/material.dart';

class ComponentMyCarList extends StatelessWidget {
  const ComponentMyCarList({super.key, required this.item});

  final MyCarListItem item;





  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.fromLTRB(10, 10, 20, 10),
        margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: colorSinbi,
          border: Border.all(
            color: colorSinbi,
            width: 3,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              children: [
                Text('차량번호 :',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'kakaoBold',
                  ),
                ),
                Text('${item.myCarNumber}',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                    fontFamily: 'kakaoBold',
                  ),
                ),
              ],
            ),
            SizedBox(height: 5,),
            Row(
              children: [
                Text('차량모델 :',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'kakaoBold',
                  ),
                ),
                Text('${item.carModel}',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                    fontFamily: 'kakaoBold',
                  ),
                ),
              ],
            ),
            SizedBox(height: 5,),
            Row(
              children: [
                Text('차량타입 :',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'kakaoBold',
                  ),
                ),
                Text('${item.carType}',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                    fontFamily: 'kakaoBold',
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
  }