import 'package:app_sinbi_apartment/model/meeting_bulletin_list_item.dart';

class MeetingBulletinListResult {
  List<MeetingBulletinListItem> list;
  int totalItemCount;
  int totalPage;
  int currentPage;
  bool isSuccess;
  int code;
  String msg;

  MeetingBulletinListResult(
      this.list, this.totalItemCount, this.totalPage, this.currentPage, this.isSuccess, this.code, this.msg
      );


  factory MeetingBulletinListResult.fromJson(Map<String, dynamic> json) {
    return MeetingBulletinListResult(
      json['list'] == null ? [] : (json['list'] as List).map((e) => MeetingBulletinListItem.fromJson(e)).toList(),
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}
