class NewsTitleListItem {
  int newsId;
  String title;
  String newsState;

  NewsTitleListItem(this.newsId, this.title, this.newsState);

  factory NewsTitleListItem.fromJson(Map<String, dynamic> json) {
    return NewsTitleListItem(
      json['newsId'],
      json['title'],
      json['newsState'],
    );
  }
}