
class MeetingRequest {
  String title;
  String content;


  MeetingRequest(this.title, this.content);

  Map<String, dynamic> toJson() {
    Map<String,dynamic> data = Map<String, dynamic>();
    data['title'] = this.title;
    data['content'] = this.content;
    return data;
  }
}