class ComplainCommentsListItem {
  int complainId;
  String username;
  String content;
  DateTime dateCreate;
  String residentGroup;

  ComplainCommentsListItem(this.complainId ,this.username, this.content,this.dateCreate, this.residentGroup);

  factory ComplainCommentsListItem.fromJson(Map<String, dynamic> json) {
    return ComplainCommentsListItem(
      json['complainId'],
      json['username'],
      json['content'],
      DateTime.parse(json['dateCreate']),
      json['residentGroup'],
    );
  }

}