import 'package:app_sinbi_apartment/model/complain_title_list.dart';

class ComplainListResult {
  List<ComplainTitleList> list;
  int totalItemCount;
  int totalPage;
  int currentPage;
  bool isSuccess;
  int code;
  String msg;

  ComplainListResult(
  this.list, this.totalItemCount, this.totalPage, this.currentPage, this.isSuccess, this.code, this.msg
  );


  factory ComplainListResult.fromJson(Map<String, dynamic> json) {
  return ComplainListResult(
  json['list'] == null ? [] : (json['list'] as List).map((e) => ComplainTitleList.fromJson(e)).toList(),
  json['totalItemCount'],
  json['totalPage'],
  json['currentPage'],
  json['isSuccess'] as bool,
  json['code'],
  json['msg'],
  );
  }
  }
