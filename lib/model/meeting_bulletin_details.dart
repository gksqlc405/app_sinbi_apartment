
class MeetingBulletinDetails {
  int meetingId;
  String title;
  DateTime dateCreate;
  String content;
  String userName;
  int aptDong;


  MeetingBulletinDetails(
      this.meetingId, this.title, this.dateCreate, this.content, this.userName, this.aptDong
      );

  factory MeetingBulletinDetails.fromJson(Map<String, dynamic> json) {
    return MeetingBulletinDetails(
      json['meetingId'],
      json['title'],
      DateTime.parse(json['dateCreate']),
      json['content'],
      json['userName'],
      json['aptDong'],
    );
  }
}

