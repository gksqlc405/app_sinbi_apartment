
import 'package:app_sinbi_apartment/model/meeting_comment_list_item.dart';

class MeetingCommentListResult {
  List<MeetingCommentListItem> list;
  int totalItemCount;
  int totalPage;
  int currentPage;
  bool isSuccess;
  int code;
  String msg;

  MeetingCommentListResult(
      this.list, this.totalItemCount, this.totalPage, this.currentPage, this.isSuccess, this.code, this.msg
      );


  factory MeetingCommentListResult.fromJson(Map<String, dynamic> json) {
    return MeetingCommentListResult(
      json['list'] == null ? [] : (json['list'] as List).map((e) => MeetingCommentListItem.fromJson(e)).toList(),
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}
