
import 'package:app_sinbi_apartment/model/news_comment_list_item.dart';

class NewsCommentListResult {
  List<NewsCommentListItem> list;
  int totalItemCount;
  int totalPage;
  int currentPage;
  bool isSuccess;
  int code;
  String msg;

  NewsCommentListResult(
      this.list, this.totalItemCount, this.totalPage, this.currentPage, this.isSuccess, this.code, this.msg
      );


  factory NewsCommentListResult.fromJson(Map<String, dynamic> json) {
    return NewsCommentListResult(
      json['list'] == null ? [] : (json['list'] as List).map((e) => NewsCommentListItem.fromJson(e)).toList(),
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}
