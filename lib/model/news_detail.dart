class NewsDetail {
  int id;
  String title;
  DateTime dateCreate;
  String content;
  String userName;
  int aptDong;


  NewsDetail(
      this.id, this.title, this.dateCreate, this.content, this.userName, this.aptDong
      );

  factory NewsDetail.fromJson(Map<String, dynamic> json) {
    return NewsDetail(
      json['id'],
      json['title'],
      DateTime.parse(json['dateCreate']),
      json['content'],
      json['userName'],
      json['aptDong'],
    );
  }
}

