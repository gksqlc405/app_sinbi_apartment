
class AdminCostDetail {
  int residentId;
  String dateStart;
  double price;


  AdminCostDetail(
      this.residentId, this.dateStart, this.price,
      );

  factory AdminCostDetail.fromJson(Map<String, dynamic> json) {
    return AdminCostDetail(
      json['residentId'],
      json['dateStart'],
      json['price'],

    );
  }
}
