class NewsCommentsRequest {
  String content;

  NewsCommentsRequest(this.content);

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data['content'] = this.content;

    return data;
  }
}