
import 'package:app_sinbi_apartment/model/news_detail.dart';

class NewsDetailResponse {
  NewsDetail date;
  bool isSuccess;
  int code;
  String msg;

  NewsDetailResponse(this.date, this.isSuccess, this.code, this.msg);

  factory NewsDetailResponse.fromJson(Map<String, dynamic> json) {
    return NewsDetailResponse(
      NewsDetail.fromJson(json['date']),
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}
