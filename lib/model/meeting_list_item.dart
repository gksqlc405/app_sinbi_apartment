

class MeetingListItem {
  String categoryValue;
  String category;
  String categoryTitle;
  String categorySubTitle;
  String image;

  MeetingListItem(this.categoryValue, this.category, this.categoryTitle, this.categorySubTitle, this.image);

  factory MeetingListItem.fromJson(Map<String, dynamic> json) {
    return MeetingListItem(
      json['categoryValue'],
      json['category'],
      json['categoryTitle'],
      json['categorySubTitle'],
      json['image'],
    );
  }
}
