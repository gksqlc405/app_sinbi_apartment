import 'package:app_sinbi_apartment/model/login_response.dart';

class LoginResponseResult {
  LoginResponse date;
  bool isSuccess;
  int code;
  String msg;

  LoginResponseResult(this.date, this.isSuccess, this.code, this.msg);

  factory LoginResponseResult.fromJson(Map<String, dynamic> json) {
    return LoginResponseResult(
      LoginResponse.fromJson(json['date']),
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}
