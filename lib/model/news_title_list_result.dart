
import 'package:app_sinbi_apartment/model/news_Title_list_item.dart';

class NewsTitleListResult {
  List<NewsTitleListItem> list;
  int totalItemCount;
  int totalPage;
  int currentPage;
  bool isSuccess;
  int code;
  String msg;

  NewsTitleListResult(
      this.list, this.totalItemCount, this.totalPage, this.currentPage, this.isSuccess, this.code, this.msg
      );


  factory NewsTitleListResult.fromJson(Map<String, dynamic> json) {
    return NewsTitleListResult(
      json['list'] == null ? [] : (json['list'] as List).map((e) => NewsTitleListItem.fromJson(e)).toList(),
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}
