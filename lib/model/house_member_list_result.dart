
import 'package:app_sinbi_apartment/model/house_member_list_item.dart';

class HouseMemberListResult {
  List<HouseMemberListItem> list;
  int totalItemCount;
  int totalPage;
  int currentPage;
  bool isSuccess;
  int code;
  String msg;

  HouseMemberListResult(
      this.list, this.totalItemCount, this.totalPage, this.currentPage, this.isSuccess, this.code, this.msg
      );


  factory HouseMemberListResult.fromJson(Map<String, dynamic> json) {
    return HouseMemberListResult(
      json['list'] == null ? [] : (json['list'] as List).map((e) => HouseMemberListItem.fromJson(e)).toList(),
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}
