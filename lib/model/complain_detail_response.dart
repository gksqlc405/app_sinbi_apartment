import 'package:app_sinbi_apartment/model/complain_detail.dart';

class ComplainDetailResponse {
  ComplainDetail date;
  bool isSuccess;
  int code;
  String msg;

  ComplainDetailResponse(this.date, this.isSuccess, this.code, this.msg);

  factory ComplainDetailResponse.fromJson(Map<String, dynamic> json) {
    return ComplainDetailResponse(
      ComplainDetail.fromJson(json['date']),
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}
