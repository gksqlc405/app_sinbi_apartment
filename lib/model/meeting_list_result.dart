
import 'package:app_sinbi_apartment/model/meeting_list_item.dart';

class MeetingListResult {
  List<MeetingListItem> list;
  int totalItemCount;
  int totalPage;
  int currentPage;
  bool isSuccess;
  int code;
  String msg;

  MeetingListResult(
      this.list, this.totalItemCount, this.totalPage, this.currentPage, this.isSuccess, this.code, this.msg
      );


  factory MeetingListResult.fromJson(Map<String, dynamic> json) {
    return MeetingListResult(
      json['list'] == null ? [] : (json['list'] as List).map((e) => MeetingListItem.fromJson(e)).toList(),
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}
