import 'package:app_sinbi_apartment/model/admin_cost_detail.dart';

class AdminCostDetailResponse {
  AdminCostDetail date;
  bool isSuccess;
  int code;
  String msg;

  AdminCostDetailResponse(this.date, this.isSuccess, this.code, this.msg);

  factory AdminCostDetailResponse.fromJson(Map<String, dynamic> json) {
    return AdminCostDetailResponse(
      AdminCostDetail.fromJson(json['date']),
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}
