class ComplainDetail {
  int id;
  String title;
  DateTime dateCreate;
  String content;
  String userName;
  int aptDong;


  ComplainDetail(
  this.id, this.title, this.dateCreate, this.content, this.userName, this.aptDong
      );

  factory ComplainDetail.fromJson(Map<String, dynamic> json) {
    return ComplainDetail(
      json['id'],
      json['title'],
     DateTime.parse(json['dateCreate']),
      json['content'],
      json['userName'],
      json['aptDong'],
    );
  }
}
