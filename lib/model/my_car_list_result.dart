
import 'package:app_sinbi_apartment/model/my_car_list-item.dart';

class MyCarListResult {
  List<MyCarListItem> list;
  int totalItemCount;
  int totalPage;
  int currentPage;
  bool isSuccess;
  int code;
  String msg;

  MyCarListResult(
      this.list, this.totalItemCount, this.totalPage, this.currentPage, this.isSuccess, this.code, this.msg
      );


  factory MyCarListResult.fromJson(Map<String, dynamic> json) {
    return MyCarListResult(
      json['list'] == null ? [] : (json['list'] as List).map((e) => MyCarListItem.fromJson(e)).toList(),
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}
