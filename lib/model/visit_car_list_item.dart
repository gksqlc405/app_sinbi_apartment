
class VisitCarListItem {
  int visitCarId;
  int residentId;
  String carNumber;
  String approvalStatus;
  DateTime dateCreate;

  VisitCarListItem(this.visitCarId ,this.residentId, this.carNumber, this.approvalStatus, this.dateCreate);

  factory VisitCarListItem.fromJson(Map<String, dynamic> json) {
    return VisitCarListItem(
      json['visitCarId'],
      json['residentId'],
      json['carNumber'],
      json['approvalStatus'],
     DateTime.parse(json['dateCreate']),
    );
  }
}