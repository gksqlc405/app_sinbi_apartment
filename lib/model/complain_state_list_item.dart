class ComplainStateListItem {
  String title;
  String serviceState;
  int complainId;

  ComplainStateListItem(this.title, this.serviceState, this.complainId);

  factory ComplainStateListItem.fromJson(Map<String, dynamic> json) {
    return ComplainStateListItem(
      json['title'],
      json['serviceState'],
      json['complainId'],
    );
  }
}
