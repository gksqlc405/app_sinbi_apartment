
class MeetingCommentListItem {
  int meetingId;
  String username;
  String content;
  DateTime dateCreate;
  String residentGroup;

  MeetingCommentListItem(this.meetingId, this.username, this.content, this.dateCreate, this.residentGroup);

  factory MeetingCommentListItem.fromJson(Map<String, dynamic> json) {
    return MeetingCommentListItem(
      json['meetingId'],
      json['username'],
      json['content'],
     DateTime.parse(json['dateCreate']),
      json['residentGroup'],


    );
  }
}
