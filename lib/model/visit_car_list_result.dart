import 'package:app_sinbi_apartment/model/visit_car_list_item.dart';

class VisitCarListResult {
  List<VisitCarListItem> list;
  int totalItemCount;
  int totalPage;
  int currentPage;
  bool isSuccess;
  int code;
  String msg;

  VisitCarListResult(
      this.list, this.totalItemCount, this.totalPage, this.currentPage, this.isSuccess, this.code, this.msg
      );


  factory VisitCarListResult.fromJson(Map<String, dynamic> json) {
    return VisitCarListResult(
      json['list'] == null ? [] : (json['list'] as List).map((e) => VisitCarListItem.fromJson(e)).toList(),
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}

