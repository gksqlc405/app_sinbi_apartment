import 'package:app_sinbi_apartment/model/complain_state_list_item.dart';
class ComplainStateListResult {
  List<ComplainStateListItem> list;
  int totalItemCount;
  int totalPage;
  int currentPage;
  bool isSuccess;
  int code;
  String msg;

  ComplainStateListResult(
      this.list, this.totalItemCount, this.totalPage, this.currentPage, this.isSuccess, this.code, this.msg
      );


  factory ComplainStateListResult.fromJson(Map<String, dynamic> json) {
    return ComplainStateListResult(
      json['list'] == null ? [] : (json['list'] as List).map((e) => ComplainStateListItem.fromJson(e)).toList(),
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}
