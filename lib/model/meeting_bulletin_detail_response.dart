
import 'package:app_sinbi_apartment/model/meeting_bulletin_details.dart';

class MeetingBulletinDetailResponse {
  MeetingBulletinDetails date;
  bool isSuccess;
  int code;
  String msg;

  MeetingBulletinDetailResponse(this.date, this.isSuccess, this.code, this.msg);

  factory MeetingBulletinDetailResponse.fromJson(Map<String, dynamic> json) {
    return MeetingBulletinDetailResponse(
      MeetingBulletinDetails.fromJson(json['date']),
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}
