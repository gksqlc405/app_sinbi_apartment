class NewsCommentListItem {
  String username;
  String content;
  DateTime dateCreate;
  String residentGroup;


  NewsCommentListItem(this.username, this.content, this.dateCreate,
      this.residentGroup);

  factory NewsCommentListItem.fromJson(Map<String, dynamic> json) {
    return NewsCommentListItem(
      json['username'],
      json['content'],
      DateTime.parse(json['dateCreate']),
      json['residentGroup'],
    );
  }
}
