
class HouseMemberListItem {
  int residentId;
  int memberId;
  String name;

  HouseMemberListItem(this.residentId, this.memberId, this.name);

  factory HouseMemberListItem.fromJson(Map<String, dynamic> json) {
    return HouseMemberListItem(
      json['residentId'],
      json['memberId'],
      json['name'],
    );
  }
}
