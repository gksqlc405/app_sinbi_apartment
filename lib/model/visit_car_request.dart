
class VisitCarRequest {
  String carNumber;


  VisitCarRequest(this.carNumber);

  Map<String, dynamic> toJson() {
    Map<String,dynamic> data = Map<String, dynamic>();
    data['carNumber'] = this.carNumber;
    return data;
  }
}