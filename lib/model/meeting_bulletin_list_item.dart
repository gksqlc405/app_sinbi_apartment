
class MeetingBulletinListItem {
  int meetingId;
  String title;

  MeetingBulletinListItem(this.meetingId ,this.title);

  factory MeetingBulletinListItem.fromJson(Map<String, dynamic> json) {
    return MeetingBulletinListItem(
      json['meetingId'],
      json['title'],

    );
  }
}
