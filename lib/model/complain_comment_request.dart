class ComplainCommentRequest {
  String content;

  ComplainCommentRequest(this.content);

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data['content'] = this.content;

    return data;
  }
}