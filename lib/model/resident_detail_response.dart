
import 'package:app_sinbi_apartment/model/resident_detail.dart';

class ResidentDetailResponse {
  ResidentDetail date;
  bool isSuccess;
  int code;
  String msg;

  ResidentDetailResponse(this.date, this.isSuccess, this.code, this.msg);

  factory ResidentDetailResponse.fromJson(Map<String, dynamic> json) {
    return ResidentDetailResponse(
      ResidentDetail.fromJson(json['date']),
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
    );
  }
}
