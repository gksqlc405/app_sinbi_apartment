
class MyCarListItem {
  int residentId;
  String myCarNumber;
  String carModel;
  String carType;

  MyCarListItem(this.residentId, this.myCarNumber, this.carModel, this.carType);

  factory MyCarListItem.fromJson(Map<String, dynamic> json) {
    return MyCarListItem(
      json['residentId'],
      json['myCarNumber'],
      json['carModel'],
      json['carType'],
    );
  }
}
