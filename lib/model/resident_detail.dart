
class ResidentDetail {
  int residentId;
  String residentName;
  String phone;
  String address;
  int aptDong;
  int aptNumber;

  ResidentDetail(
      this.residentId, this.residentName, this.phone, this.address, this.aptDong, this.aptNumber,
      );

  factory ResidentDetail.fromJson(Map<String, dynamic> json) {
    return ResidentDetail(
      json['residentId'],
      json['residentName'],
      json['phone'],
      json['address'],
      json['aptDong'],
      json['aptNumber'],
    );
  }
}
