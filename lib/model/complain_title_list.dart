class ComplainTitleList {
  String title;
  String serviceState;
  int complainId;

  ComplainTitleList(this.title, this.serviceState, this.complainId);

  factory ComplainTitleList.fromJson(Map<String, dynamic> json) {
    return ComplainTitleList(
      json['title'],
      json['serviceState'],
      json['complainId'],
    );
  }
}